<?php

/* extension/basel/panel_tabs/custom-javascript.twig */
class __TwigTemplate_9532f28efabb8f40d0e2a07ae98f5a3a77f48a9b4e3848e8ab7ce2258c27bb99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Custom Javascript</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Custom Javascript Status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_custom_js_status]\" class=\"custom-js-select\" value=\"0\" ";
        // line 6
        if (((isset($context["basel_custom_js_status"]) ? $context["basel_custom_js_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_custom_js_status]\" class=\"custom-js-select\" value=\"1\" ";
        // line 7
        if (((isset($context["basel_custom_js_status"]) ? $context["basel_custom_js_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div id=\"custom_js_holder\"";
        // line 11
        if ((isset($context["basel_custom_js_status"]) ? $context["basel_custom_js_status"] : null)) {
            echo " style=\"display:block\"";
        } else {
            echo " style=\"display:none\"";
        }
        echo ">
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Javascript</label>
    <div class=\"col-sm-10\">
    <textarea name=\"settings[basel][basel_custom_js]\" class=\"form-control code\">";
        // line 15
        echo (((isset($context["basel_custom_js"]) ? $context["basel_custom_js"] : null)) ? ((isset($context["basel_custom_js"]) ? $context["basel_custom_js"] : null)) : (""));
        echo "</textarea>
    </div>                   
</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/custom-javascript.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 15,  41 => 11,  32 => 7,  26 => 6,  19 => 1,);
    }
}
/* <legend>Custom Javascript</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Custom Javascript Status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_custom_js_status]" class="custom-js-select" value="0" {% if basel_custom_js_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_custom_js_status]" class="custom-js-select" value="1" {% if basel_custom_js_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div id="custom_js_holder"{% if basel_custom_js_status %} style="display:block"{% else %} style="display:none"{% endif %}>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Javascript</label>*/
/*     <div class="col-sm-10">*/
/*     <textarea name="settings[basel][basel_custom_js]" class="form-control code">{{ basel_custom_js ? basel_custom_js }}</textarea>*/
/*     </div>                   */
/* </div>*/
/* </div>*/
/* */
