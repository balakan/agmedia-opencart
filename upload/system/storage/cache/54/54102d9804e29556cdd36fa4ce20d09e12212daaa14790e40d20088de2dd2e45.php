<?php

/* extension/basel/basel.twig */
class __TwigTemplate_7f6c4a617b55e838e902395db3f36ba1d3a6e769fe6f1ef87f7d9e90983f632f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "

<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">  
    
      <div class=\"pull-right\">
\t<button type=\"submit\" form=\"form-basel\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\">Save</button>
    <a class=\"btn btn-default\" href=\"http://basel.openthemer.com/help\" target=\"_blank\" data-toggle=\"tooltip\" title=\"View Theme Documentation\">Documentation</a>
    </div>
      <h1>";
        // line 11
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "      </ul>
    </div>
  </div>
  
  <div class=\"container-fluid\">    
    ";
        // line 21
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 22
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_permission"]) ? $context["error_permission"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 26
        echo "
    ";
        // line 27
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 28
            echo "    <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 32
        echo "  
   <form action=\"";
        // line 33
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-basel\" class=\"form-horizontal\">
   
   <div class=\"panel-wrapper\">
   
   <div class=\"panel-left\">
   <div class=\"setting-header\">v 1.2.9.0</div>
   <ul class=\"menu list-unstyled\">
   <li class=\"active\"><a href=\"#tab-status\" data-toggle=\"tab\"><i class=\"fa fa-toggle-on\"></i> Status</a></li>
   <li><a href=\"#tab-shop\" data-toggle=\"tab\"><i class=\"fa fa-shopping-cart\"></i> Shop Settings</a></li>
   <li><a href=\"#tab-header\" data-toggle=\"tab\"><i class=\"fa fa-columns\"></i> Header</a></li>
   <li><a href=\"#tab-page-titles\" data-toggle=\"tab\"><i class=\"fa fa-chain\"></i> Breadcrumbs</a></li>
   <li><a href=\"#tab-product-pages\" data-toggle=\"tab\"><i class=\"fa fa-clone\"></i> Product Pages</a></li>
   <li><a href=\"#tab-category-pages\" data-toggle=\"tab\"><i class=\"fa fa-th\"></i> Category Pages</a></li>
   <li><a href=\"#tab-contact-page\" data-toggle=\"tab\"><i class=\"fa fa-envelope-o\"></i> Contact Page</a></li>
   <li><a href=\"#tab-footer\" data-toggle=\"tab\"><i class=\"fa fa-gears\"></i> Footer</a></li>
   <li><a href=\"#tab-notifications\" data-toggle=\"tab\"><i class=\"fa fa-comment-o\"></i> Notifications</a></li>
   <li><a href=\"#tab-styles\" data-toggle=\"tab\"><i class=\"fa fa-paint-brush\"></i> Design & Colors</a></li>
   <li><a href=\"#tab-typography\" data-toggle=\"tab\"><i class=\"fa fa-font\"></i> Typography</a></li>
   <li><a href=\"#tab-image-sizes\" data-toggle=\"tab\"><i class=\"fa fa-image\"></i> Image Sizes</a></li>
   <li><a href=\"#tab-custom-css\" data-toggle=\"tab\"><i class=\"fa fa-css3\"></i> Custom CSS</a></li>
   <li><a href=\"#tab-custom-javascript\" data-toggle=\"tab\"><i class=\"fa fa-code\"></i> Custom Javascript</a></li>
   <li><a href=\"#tab-one-click-installer\" data-toggle=\"tab\"><i class=\"fa fa-magic\"></i> One Click Installer</a></li>
   
   </ul>
   </div><!-- .panel-left ends -->
   
   <div class=\"panel-right\">
   <div class=\"store-header\">
   <div class=\"form-group\">
    <label class=\"col-sm-9 control-label\">Store to edit:</label>
        <div class=\"col-sm-3\">
            <select name=\"store_id\" class=\"form-control\">
                ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            // line 66
            echo "                    ";
            if (($this->getAttribute($context["result"], "store_id", array()) == (isset($context["store_id"]) ? $context["store_id"] : null))) {
                // line 67
                echo "                        <option value=\"";
                echo $this->getAttribute($context["result"], "store_id", array());
                echo "\" selected=\"selected\">
                            ";
                // line 68
                echo $this->getAttribute($context["result"], "name", array());
                echo "
                        </option>
                    ";
            } else {
                // line 71
                echo "                        <option value=\"";
                echo $this->getAttribute($context["result"], "store_id", array());
                echo "\">
                            ";
                // line 72
                echo $this->getAttribute($context["result"], "name", array());
                echo "
                        </option>
                    ";
            }
            // line 75
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "            </select>
        </div>
    </div>
   </div>
   
   
   
   <div class=\"main-content\">
   
   <div class=\"tab-content\">
   
       <div class=\"tab-pane active\" id=\"tab-status\">
       ";
        // line 88
        $this->loadTemplate("extension/basel/panel_tabs/status.twig", "extension/basel/basel.twig", 88)->display($context);
        // line 89
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-shop\">
       ";
        // line 92
        $this->loadTemplate("extension/basel/panel_tabs/shop.twig", "extension/basel/basel.twig", 92)->display($context);
        // line 93
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-header\">
       ";
        // line 96
        $this->loadTemplate("extension/basel/panel_tabs/header.twig", "extension/basel/basel.twig", 96)->display($context);
        // line 97
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-page-titles\">
       ";
        // line 100
        $this->loadTemplate("extension/basel/panel_tabs/page-titles.twig", "extension/basel/basel.twig", 100)->display($context);
        // line 101
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-product-pages\">
       ";
        // line 104
        $this->loadTemplate("extension/basel/panel_tabs/product-pages.twig", "extension/basel/basel.twig", 104)->display($context);
        // line 105
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-category-pages\">
       ";
        // line 108
        $this->loadTemplate("extension/basel/panel_tabs/category-pages.twig", "extension/basel/basel.twig", 108)->display($context);
        // line 109
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-contact-page\">
       ";
        // line 112
        $this->loadTemplate("extension/basel/panel_tabs/contact-page.twig", "extension/basel/basel.twig", 112)->display($context);
        // line 113
        echo "       </div>

       <div class=\"tab-pane\" id=\"tab-footer\">
       ";
        // line 116
        $this->loadTemplate("extension/basel/panel_tabs/footer.twig", "extension/basel/basel.twig", 116)->display($context);
        // line 117
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-notifications\">
       ";
        // line 120
        $this->loadTemplate("extension/basel/panel_tabs/notifications.twig", "extension/basel/basel.twig", 120)->display($context);
        // line 121
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-styles\">
       ";
        // line 124
        $this->loadTemplate("extension/basel/panel_tabs/styles.twig", "extension/basel/basel.twig", 124)->display($context);
        // line 125
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-typography\">
       ";
        // line 128
        $this->loadTemplate("extension/basel/panel_tabs/typography.twig", "extension/basel/basel.twig", 128)->display($context);
        // line 129
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-image-sizes\">
       ";
        // line 132
        $this->loadTemplate("extension/basel/panel_tabs/image-sizes.twig", "extension/basel/basel.twig", 132)->display($context);
        // line 133
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-custom-css\">
       ";
        // line 136
        $this->loadTemplate("extension/basel/panel_tabs/custom-css.twig", "extension/basel/basel.twig", 136)->display($context);
        // line 137
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-custom-javascript\">
       ";
        // line 140
        $this->loadTemplate("extension/basel/panel_tabs/custom-javascript.twig", "extension/basel/basel.twig", 140)->display($context);
        // line 141
        echo "       </div>
       
       <div class=\"tab-pane\" id=\"tab-one-click-installer\">
       ";
        // line 144
        $this->loadTemplate("extension/basel/panel_tabs/one-click-installer.twig", "extension/basel/basel.twig", 144)->display($context);
        // line 145
        echo "       </div>
       
    </div>
   
   </div> <!-- .main-content ends -->
    
  </div> <!-- panel-right ends -->
     
 </div>        
</form>
</div> <!-- content ends -->


<script type=\"text/javascript\"><!--
// Store selector
\$('select[name=\\'store_id\\']').on('change', function () {
    location = 'index.php?route=extension/basel/basel&user_token=";
        // line 161
        echo (isset($context["token"]) ? $context["token"] : null);
        echo "&store_id=' +  encodeURIComponent(\$(this).val());
});
//--></script>
<script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
<link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
<script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script>
<script type=\"text/javascript\">
// Header preview image
\$('#header-select').on('change', function() {
\$('#header-preview img').attr('src', 'view/javascript/basel/img/theme-panel/headers/' + \$(this).val() + '.png');
});
// Always open first tab
\$('.nav-tabs').each(function() {
\t\$(this).find('li:first a').tab('show');
});
// Static links on/off
\$('.links-select').on('change', function() {
if (\$(this).val() == '1') {
\t\$('#custom_links_holder').css('display', 'block');
} else {
\t\$('#custom_links_holder').css('display', 'none');
}
});
// Custom design on/off
\$('.design-select').on('change', function() {
if (\$(this).val() == '1') {
\t\$('#custom_design_holder').css('display', 'block');
} else {
\t\$('#custom_design_holder').css('display', 'none');
}
});
// Custom fonts on/off
\$('.typo-select').on('change', function() {
if (\$(this).val() == '1') {
\t\$('#basel_typo_holder').css('display', 'block');
} else {
\t\$('#basel_typo_holder').css('display', 'none');
}
});
// Custom footer links on/off
\$('.footer-custom-links-select').on('change', function() {
if (\$(this).val() == '1') {
\t\$('#custom_footer_links_holder').css('display', 'block');
} else {
\t\$('#custom_footer_links_holder').css('display', 'none');
}
});
// Custom CSS on/off
\$('.custom-css-select').on('change', function() {
if (\$(this).val() == '1') {
\t\$('#custom_css_holder').css('display', 'block');
} else {
\t\$('#custom_css_holder').css('display', 'none');
}
});
// Custom Javascript on/off
\$('.custom-js-select').on('change', function() {
if (\$(this).val() == '1') {
\t\$('#custom_js_holder').css('display', 'block');
} else {
\t\$('#custom_js_holder').css('display', 'none');
}
});
// Colorpicker 
\$('.colorfield input').colorpicker({
sliders: {
saturation: {maxLeft: 150, maxTop: 150},hue: { maxTop: 150},alpha: { maxTop: 150}}
}).on('changeColor.colorpicker', function(){
\$(this).parent().find('.input-group-addon i').css(\"background-color\", \$(this).val());
});
var enable_editor = function(area, lang_id) {
\tif ( \$('#enable-editor-' + area + '-' + lang_id + '').hasClass('active') ) {
\t\t\$('#enable-editor-' + area + '-' + lang_id + '').text('Enable HTML editor').removeClass('active');
\t\t\$('#editor-textarea-' + area + '-' + lang_id + '').summernote('destroy');
\t} else {
\t\t\$('#enable-editor-' + area + '-' + lang_id + '').text('Disable HTML editor').addClass('active');
\t\t\$('#editor-textarea-' + area + '-' + lang_id + '').summernote({
\t\t\tdisableDragAndDrop: true,
\t\t\tstyleWithSpan: false,
\t\t\theight: 260,
\t\t\temptyPara: '',
\t\t\tcodemirror: { // codemirror options
\t\t\t\tmode: 'text/html',
\t\t\t\thtmlMode: true,
\t\t\t\tlineNumbers: true,
\t\t\t\ttheme: 'monokai'
\t\t\t},\t\t\t
\t\t\tfontsize: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '36', '48' , '64'],
\t\t\ttoolbar: [
\t\t\t\t['style', ['style']],
\t\t\t\t['font', [ \"underline\",\"italic\", \"bold\", \"clear\"]],
\t\t\t\t['fontsize', ['fontsize']],
\t\t\t\t['color', ['color']],
\t\t\t\t['para', ['paragraph']],
\t\t\t\t['insert', ['link', 'image']],
\t\t\t\t['view', ['fullscreen', 'codeview']]
\t\t\t],
\t\t\tcallbacks: {
\t\t\t\t\tonPaste: function (e) {
\t\t\t\t\tvar bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
\t\t\t\t\te.preventDefault();
\t\t\t\t\tdocument.execCommand('insertText', false, bufferText);
\t\t\t\t\t}
            },
\t\t\tpopover: {
           \t\timage: [
\t\t\t\t\t['custom', ['imageAttributes']],
\t\t\t\t\t['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
\t\t\t\t\t['float', ['floatLeft', 'floatRight', 'floatNone']],
\t\t\t\t\t['remove', ['removeMedia']]
\t\t\t\t],
\t\t\t},
\t\t\tbuttons: {
    \t\t\timage: function() {
\t\t\t\t\tvar ui = \$.summernote.ui;
\t\t\t\t\t// create button
\t\t\t\t\tvar button = ui.button({
\t\t\t\t\t\tcontents: '<i class=\"fa fa-image\" />',
\t\t\t\t\t\ttooltip: \$.summernote.lang[\$.summernote.options.lang].image.image,
\t\t\t\t\t\tclick: function () {
\t\t\t\t\t\t\t\$('#modal-image').remove();
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl: 'index.php?route=common/filemanager&user_token=' + getURLVar('user_token'),
\t\t\t\t\t\t\t\tdataType: 'html',
\t\t\t\t\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\t\t\t\t\$('#button-image i').replaceWith('<i class=\"fa fa-circle-o-notch fa-spin\"></i>');
\t\t\t\t\t\t\t\t\t\$('#button-image').prop('disabled', true);
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcomplete: function() {
\t\t\t\t\t\t\t\t\t\$('#button-image i').replaceWith('<i class=\"fa fa-upload\"></i>');
\t\t\t\t\t\t\t\t\t\$('#button-image').prop('disabled', false);
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tsuccess: function(html) {
\t\t\t\t\t\t\t\t\t\$('body').append('<div id=\"modal-image\" class=\"modal\">' + html + '</div>');
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\$('#modal-image').modal('show');
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\$('#modal-image').delegate('a.thumbnail', 'click', function(e) {
\t\t\t\t\t\t\t\t\t\te.preventDefault();
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\$('#editor-textarea-' + area + '-' + lang_id + '').summernote('insertImage', \$(this).attr('href'));
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\$('#modal-image').modal('hide');
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t}});}});
\t\t\t\treturn button.render();
\t\t}}});
\t}
}
</script>
</div>
";
        // line 312
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/basel/basel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  445 => 312,  291 => 161,  273 => 145,  271 => 144,  266 => 141,  264 => 140,  259 => 137,  257 => 136,  252 => 133,  250 => 132,  245 => 129,  243 => 128,  238 => 125,  236 => 124,  231 => 121,  229 => 120,  224 => 117,  222 => 116,  217 => 113,  215 => 112,  210 => 109,  208 => 108,  203 => 105,  201 => 104,  196 => 101,  194 => 100,  189 => 97,  187 => 96,  182 => 93,  180 => 92,  175 => 89,  173 => 88,  159 => 76,  153 => 75,  147 => 72,  142 => 71,  136 => 68,  131 => 67,  128 => 66,  124 => 65,  89 => 33,  86 => 32,  78 => 28,  76 => 27,  73 => 26,  65 => 22,  63 => 21,  56 => 16,  45 => 14,  41 => 13,  36 => 11,  30 => 8,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">  */
/*     */
/*       <div class="pull-right">*/
/* 	<button type="submit" form="form-basel" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary">Save</button>*/
/*     <a class="btn btn-default" href="http://basel.openthemer.com/help" target="_blank" data-toggle="tooltip" title="View Theme Documentation">Documentation</a>*/
/*     </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   */
/*   <div class="container-fluid">    */
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_permission }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/* */
/*     {% if success %}*/
/*     <div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*   */
/*    <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-basel" class="form-horizontal">*/
/*    */
/*    <div class="panel-wrapper">*/
/*    */
/*    <div class="panel-left">*/
/*    <div class="setting-header">v 1.2.9.0</div>*/
/*    <ul class="menu list-unstyled">*/
/*    <li class="active"><a href="#tab-status" data-toggle="tab"><i class="fa fa-toggle-on"></i> Status</a></li>*/
/*    <li><a href="#tab-shop" data-toggle="tab"><i class="fa fa-shopping-cart"></i> Shop Settings</a></li>*/
/*    <li><a href="#tab-header" data-toggle="tab"><i class="fa fa-columns"></i> Header</a></li>*/
/*    <li><a href="#tab-page-titles" data-toggle="tab"><i class="fa fa-chain"></i> Breadcrumbs</a></li>*/
/*    <li><a href="#tab-product-pages" data-toggle="tab"><i class="fa fa-clone"></i> Product Pages</a></li>*/
/*    <li><a href="#tab-category-pages" data-toggle="tab"><i class="fa fa-th"></i> Category Pages</a></li>*/
/*    <li><a href="#tab-contact-page" data-toggle="tab"><i class="fa fa-envelope-o"></i> Contact Page</a></li>*/
/*    <li><a href="#tab-footer" data-toggle="tab"><i class="fa fa-gears"></i> Footer</a></li>*/
/*    <li><a href="#tab-notifications" data-toggle="tab"><i class="fa fa-comment-o"></i> Notifications</a></li>*/
/*    <li><a href="#tab-styles" data-toggle="tab"><i class="fa fa-paint-brush"></i> Design & Colors</a></li>*/
/*    <li><a href="#tab-typography" data-toggle="tab"><i class="fa fa-font"></i> Typography</a></li>*/
/*    <li><a href="#tab-image-sizes" data-toggle="tab"><i class="fa fa-image"></i> Image Sizes</a></li>*/
/*    <li><a href="#tab-custom-css" data-toggle="tab"><i class="fa fa-css3"></i> Custom CSS</a></li>*/
/*    <li><a href="#tab-custom-javascript" data-toggle="tab"><i class="fa fa-code"></i> Custom Javascript</a></li>*/
/*    <li><a href="#tab-one-click-installer" data-toggle="tab"><i class="fa fa-magic"></i> One Click Installer</a></li>*/
/*    */
/*    </ul>*/
/*    </div><!-- .panel-left ends -->*/
/*    */
/*    <div class="panel-right">*/
/*    <div class="store-header">*/
/*    <div class="form-group">*/
/*     <label class="col-sm-9 control-label">Store to edit:</label>*/
/*         <div class="col-sm-3">*/
/*             <select name="store_id" class="form-control">*/
/*                 {% for result in stores %}*/
/*                     {% if result.store_id == store_id %}*/
/*                         <option value="{{ result.store_id }}" selected="selected">*/
/*                             {{ result.name }}*/
/*                         </option>*/
/*                     {% else %}*/
/*                         <option value="{{ result.store_id }}">*/
/*                             {{ result.name }}*/
/*                         </option>*/
/*                     {% endif %}*/
/*                 {% endfor %}*/
/*             </select>*/
/*         </div>*/
/*     </div>*/
/*    </div>*/
/*    */
/*    */
/*    */
/*    <div class="main-content">*/
/*    */
/*    <div class="tab-content">*/
/*    */
/*        <div class="tab-pane active" id="tab-status">*/
/*        {% include 'extension/basel/panel_tabs/status.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-shop">*/
/*        {% include 'extension/basel/panel_tabs/shop.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-header">*/
/*        {% include 'extension/basel/panel_tabs/header.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-page-titles">*/
/*        {% include 'extension/basel/panel_tabs/page-titles.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-product-pages">*/
/*        {% include 'extension/basel/panel_tabs/product-pages.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-category-pages">*/
/*        {% include 'extension/basel/panel_tabs/category-pages.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-contact-page">*/
/*        {% include 'extension/basel/panel_tabs/contact-page.twig' %}*/
/*        </div>*/
/* */
/*        <div class="tab-pane" id="tab-footer">*/
/*        {% include 'extension/basel/panel_tabs/footer.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-notifications">*/
/*        {% include 'extension/basel/panel_tabs/notifications.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-styles">*/
/*        {% include 'extension/basel/panel_tabs/styles.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-typography">*/
/*        {% include 'extension/basel/panel_tabs/typography.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-image-sizes">*/
/*        {% include 'extension/basel/panel_tabs/image-sizes.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-custom-css">*/
/*        {% include 'extension/basel/panel_tabs/custom-css.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-custom-javascript">*/
/*        {% include 'extension/basel/panel_tabs/custom-javascript.twig' %}*/
/*        </div>*/
/*        */
/*        <div class="tab-pane" id="tab-one-click-installer">*/
/*        {% include 'extension/basel/panel_tabs/one-click-installer.twig' %}*/
/*        </div>*/
/*        */
/*     </div>*/
/*    */
/*    </div> <!-- .main-content ends -->*/
/*     */
/*   </div> <!-- panel-right ends -->*/
/*      */
/*  </div>        */
/* </form>*/
/* </div> <!-- content ends -->*/
/* */
/* */
/* <script type="text/javascript"><!--*/
/* // Store selector*/
/* $('select[name=\'store_id\']').on('change', function () {*/
/*     location = 'index.php?route=extension/basel/basel&user_token={{ token }}&store_id=' +  encodeURIComponent($(this).val());*/
/* });*/
/* //--></script>*/
/* <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>*/
/* <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />*/
/* <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>*/
/* <script type="text/javascript">*/
/* // Header preview image*/
/* $('#header-select').on('change', function() {*/
/* $('#header-preview img').attr('src', 'view/javascript/basel/img/theme-panel/headers/' + $(this).val() + '.png');*/
/* });*/
/* // Always open first tab*/
/* $('.nav-tabs').each(function() {*/
/* 	$(this).find('li:first a').tab('show');*/
/* });*/
/* // Static links on/off*/
/* $('.links-select').on('change', function() {*/
/* if ($(this).val() == '1') {*/
/* 	$('#custom_links_holder').css('display', 'block');*/
/* } else {*/
/* 	$('#custom_links_holder').css('display', 'none');*/
/* }*/
/* });*/
/* // Custom design on/off*/
/* $('.design-select').on('change', function() {*/
/* if ($(this).val() == '1') {*/
/* 	$('#custom_design_holder').css('display', 'block');*/
/* } else {*/
/* 	$('#custom_design_holder').css('display', 'none');*/
/* }*/
/* });*/
/* // Custom fonts on/off*/
/* $('.typo-select').on('change', function() {*/
/* if ($(this).val() == '1') {*/
/* 	$('#basel_typo_holder').css('display', 'block');*/
/* } else {*/
/* 	$('#basel_typo_holder').css('display', 'none');*/
/* }*/
/* });*/
/* // Custom footer links on/off*/
/* $('.footer-custom-links-select').on('change', function() {*/
/* if ($(this).val() == '1') {*/
/* 	$('#custom_footer_links_holder').css('display', 'block');*/
/* } else {*/
/* 	$('#custom_footer_links_holder').css('display', 'none');*/
/* }*/
/* });*/
/* // Custom CSS on/off*/
/* $('.custom-css-select').on('change', function() {*/
/* if ($(this).val() == '1') {*/
/* 	$('#custom_css_holder').css('display', 'block');*/
/* } else {*/
/* 	$('#custom_css_holder').css('display', 'none');*/
/* }*/
/* });*/
/* // Custom Javascript on/off*/
/* $('.custom-js-select').on('change', function() {*/
/* if ($(this).val() == '1') {*/
/* 	$('#custom_js_holder').css('display', 'block');*/
/* } else {*/
/* 	$('#custom_js_holder').css('display', 'none');*/
/* }*/
/* });*/
/* // Colorpicker */
/* $('.colorfield input').colorpicker({*/
/* sliders: {*/
/* saturation: {maxLeft: 150, maxTop: 150},hue: { maxTop: 150},alpha: { maxTop: 150}}*/
/* }).on('changeColor.colorpicker', function(){*/
/* $(this).parent().find('.input-group-addon i').css("background-color", $(this).val());*/
/* });*/
/* var enable_editor = function(area, lang_id) {*/
/* 	if ( $('#enable-editor-' + area + '-' + lang_id + '').hasClass('active') ) {*/
/* 		$('#enable-editor-' + area + '-' + lang_id + '').text('Enable HTML editor').removeClass('active');*/
/* 		$('#editor-textarea-' + area + '-' + lang_id + '').summernote('destroy');*/
/* 	} else {*/
/* 		$('#enable-editor-' + area + '-' + lang_id + '').text('Disable HTML editor').addClass('active');*/
/* 		$('#editor-textarea-' + area + '-' + lang_id + '').summernote({*/
/* 			disableDragAndDrop: true,*/
/* 			styleWithSpan: false,*/
/* 			height: 260,*/
/* 			emptyPara: '',*/
/* 			codemirror: { // codemirror options*/
/* 				mode: 'text/html',*/
/* 				htmlMode: true,*/
/* 				lineNumbers: true,*/
/* 				theme: 'monokai'*/
/* 			},			*/
/* 			fontsize: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '30', '36', '48' , '64'],*/
/* 			toolbar: [*/
/* 				['style', ['style']],*/
/* 				['font', [ "underline","italic", "bold", "clear"]],*/
/* 				['fontsize', ['fontsize']],*/
/* 				['color', ['color']],*/
/* 				['para', ['paragraph']],*/
/* 				['insert', ['link', 'image']],*/
/* 				['view', ['fullscreen', 'codeview']]*/
/* 			],*/
/* 			callbacks: {*/
/* 					onPaste: function (e) {*/
/* 					var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');*/
/* 					e.preventDefault();*/
/* 					document.execCommand('insertText', false, bufferText);*/
/* 					}*/
/*             },*/
/* 			popover: {*/
/*            		image: [*/
/* 					['custom', ['imageAttributes']],*/
/* 					['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],*/
/* 					['float', ['floatLeft', 'floatRight', 'floatNone']],*/
/* 					['remove', ['removeMedia']]*/
/* 				],*/
/* 			},*/
/* 			buttons: {*/
/*     			image: function() {*/
/* 					var ui = $.summernote.ui;*/
/* 					// create button*/
/* 					var button = ui.button({*/
/* 						contents: '<i class="fa fa-image" />',*/
/* 						tooltip: $.summernote.lang[$.summernote.options.lang].image.image,*/
/* 						click: function () {*/
/* 							$('#modal-image').remove();*/
/* 							$.ajax({*/
/* 								url: 'index.php?route=common/filemanager&user_token=' + getURLVar('user_token'),*/
/* 								dataType: 'html',*/
/* 								beforeSend: function() {*/
/* 									$('#button-image i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');*/
/* 									$('#button-image').prop('disabled', true);*/
/* 								},*/
/* 								complete: function() {*/
/* 									$('#button-image i').replaceWith('<i class="fa fa-upload"></i>');*/
/* 									$('#button-image').prop('disabled', false);*/
/* 								},*/
/* 								success: function(html) {*/
/* 									$('body').append('<div id="modal-image" class="modal">' + html + '</div>');*/
/* 									*/
/* 									$('#modal-image').modal('show');*/
/* 									*/
/* 									$('#modal-image').delegate('a.thumbnail', 'click', function(e) {*/
/* 										e.preventDefault();*/
/* 										*/
/* 										$('#editor-textarea-' + area + '-' + lang_id + '').summernote('insertImage', $(this).attr('href'));*/
/* 																	*/
/* 										$('#modal-image').modal('hide');*/
/* 									});*/
/* 								}});}});*/
/* 				return button.render();*/
/* 		}}});*/
/* 	}*/
/* }*/
/* </script>*/
/* </div>*/
/* {{ footer }}*/
