<?php

/* basel/template/common/headers/header1.twig */
class __TwigTemplate_c5499de53ad0671e41dba126d8244e08421c6be1f92b28a467bcbccf8f33a46c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"header-wrapper header1 fixed-header-possible\">
";
        // line 2
        if ((isset($context["top_line_style"]) ? $context["top_line_style"] : null)) {
            // line 3
            echo "<div class=\"top_line\">
  <div class=\"container ";
            // line 4
            echo (isset($context["top_line_width"]) ? $context["top_line_width"] : null);
            echo "\">
  \t<div class=\"table\">
        <div class=\"table-cell left sm-text-center xs-text-center\">
            <div class=\"promo-message\">";
            // line 7
            echo (isset($context["promo_message"]) ? $context["promo_message"] : null);
            echo "</div>
        </div>
        <div class=\"table-cell text-right hidden-xs hidden-sm\">
            <div class=\"links\">
            <ul>
            ";
            // line 12
            $this->loadTemplate("basel/template/common/static_links.twig", "basel/template/common/headers/header1.twig", 12)->display($context);
            // line 13
            echo "            </ul>
            ";
            // line 14
            if ((isset($context["lang_curr_title"]) ? $context["lang_curr_title"] : null)) {
                // line 15
                echo "            <div class=\"setting-ul\">
            <div class=\"setting-li dropdown-wrapper from-left lang-curr-trigger nowrap\"><a>
            <span>";
                // line 17
                echo (isset($context["lang_curr_title"]) ? $context["lang_curr_title"] : null);
                echo "</span>
            </a>
            <div class=\"dropdown-content dropdown-right lang-curr-wrapper\">
            ";
                // line 20
                echo (isset($context["language"]) ? $context["language"] : null);
                echo "
            ";
                // line 21
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "
            </div>
            </div>
            </div>
            ";
            }
            // line 26
            echo "            </div>
        </div>
    </div> <!-- .table ends -->
  </div> <!-- .container ends -->
</div> <!-- .top_line ends -->
";
        }
        // line 32
        echo "<span class=\"table header-main sticky-header-placeholder\">&nbsp;</span>
<div class=\"sticky-header outer-container header-style\">
<div class=\"container ";
        // line 34
        echo (isset($context["main_header_width"]) ? $context["main_header_width"] : null);
        echo "\">
<div class=\"table header-main\">
<div class=\"table-cell w40 menu-cell hidden-xs hidden-sm\">
";
        // line 37
        if ((isset($context["primary_menu"]) ? $context["primary_menu"] : null)) {
            // line 38
            echo "<div class=\"main-menu menu-stay-left\">
\t<ul class=\"categories\">
\t\t";
            // line 40
            if (((isset($context["primary_menu"]) ? $context["primary_menu"] : null) == "oc")) {
                // line 41
                echo "        <!-- Default menu -->
        ";
                // line 42
                echo (isset($context["default_menu"]) ? $context["default_menu"] : null);
                echo "
      ";
            } elseif (            // line 43
array_key_exists("primary_menu", $context)) {
                // line 44
                echo "        <!-- Mega menu -->
        ";
                // line 45
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["primary_menu_desktop"]) ? $context["primary_menu_desktop"] : null));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["key"] => $context["row"]) {
                    // line 46
                    echo "        ";
                    $this->loadTemplate("basel/template/common/menus/mega_menu.twig", "basel/template/common/headers/header1.twig", 46)->display($context);
                    // line 47
                    echo "        ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "      ";
            }
            // line 49
            echo "    </ul>
</div>
";
        }
        // line 52
        echo "</div>
<div class=\"table-cell w20 logo text-center\">
    ";
        // line 54
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 55
            echo "    <div id=\"logo\">
    <a href=\"";
            // line 56
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" /></a>
    </div>
    ";
        }
        // line 59
        echo "</div>
<div class=\"table-cell w40 shortcuts text-right\"> 
   <div class=\"font-zero\">
    ";
        // line 62
        if ((isset($context["header_login"]) ? $context["header_login"] : null)) {
            // line 63
            echo "        <div class=\"shortcut-wrapper sign-in hidden-sx hidden-sm hidden-xs\">
        ";
            // line 64
            if ((isset($context["logged"]) ? $context["logged"] : null)) {
                // line 65
                echo "        <a class=\"anim-underline\" href=\"";
                echo (isset($context["account"]) ? $context["account"] : null);
                echo "\">";
                echo (isset($context["text_account"]) ? $context["text_account"] : null);
                echo "</a> &nbsp;/&nbsp; 
        <a class=\"anim-underline\" href=\"";
                // line 66
                echo (isset($context["logout"]) ? $context["logout"] : null);
                echo "\">";
                echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
                echo "</a>
        ";
            } else {
                // line 68
                echo "        <a class=\"anim-underline\" href=\"";
                echo (isset($context["login"]) ? $context["login"] : null);
                echo "\">";
                echo (isset($context["text_login"]) ? $context["text_login"] : null);
                echo " / ";
                echo (isset($context["text_register"]) ? $context["text_register"] : null);
                echo "</a>
        ";
            }
            // line 70
            echo "        </div>
    ";
        }
        // line 72
        echo "    ";
        if ((isset($context["header_search"]) ? $context["header_search"] : null)) {
            // line 73
            echo "    <div class=\"icon-element\">
    <div class=\"dropdown-wrapper-click from-top hidden-sx hidden-sm hidden-xs\">
    <a class=\"shortcut-wrapper search-trigger from-top clicker\">
    <i class=\"icon-magnifier icon\"></i>
    </a>
    <div class=\"dropdown-content dropdown-right\">
    <div class=\"search-dropdown-holder\">
    <div class=\"search-holder\">
    ";
            // line 81
            echo (isset($context["basel_search"]) ? $context["basel_search"] : null);
            echo "
    </div>
    </div>
    </div>
    </div>
    </div>
    ";
        }
        // line 88
        echo "    <div class=\"icon-element is_wishlist\">
    <a class=\"shortcut-wrapper wishlist\" href=\"";
        // line 89
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\">
    <div class=\"wishlist-hover\"><i class=\"icon-heart icon\"></i><span class=\"counter wishlist-counter\">";
        // line 90
        echo (isset($context["wishlist_counter"]) ? $context["wishlist_counter"] : null);
        echo "</span></div>
    </a>
    </div>
    <div class=\"icon-element catalog_hide\">
    <div id=\"cart\" class=\"dropdown-wrapper from-top\">
    <a href=\"";
        // line 95
        echo (isset($context["shopping_cart"]) ? $context["shopping_cart"] : null);
        echo "\" class=\"shortcut-wrapper cart\">
    <i id=\"cart-icon\" class=\"global-cart icon\"></i> <span id=\"cart-total\" class=\"nowrap\">
    <span class=\"counter cart-total-items\">";
        // line 97
        echo (isset($context["cart_items"]) ? $context["cart_items"] : null);
        echo "</span> <span class=\"slash hidden-md hidden-sm hidden-xs\">/</span>&nbsp;<b class=\"cart-total-amount hidden-sm hidden-xs\">";
        echo (isset($context["cart_amount"]) ? $context["cart_amount"] : null);
        echo "</b>
    </span>
    </a>
    <div class=\"dropdown-content dropdown-right hidden-sm hidden-xs\">
    ";
        // line 101
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
    </div>
    </div>
    </div>
    <div class=\"icon-element\">
    <a class=\"shortcut-wrapper menu-trigger hidden-md hidden-lg\">
    <i class=\"icon-line-menu icon\"></i>
    </a>
    </div>
   </div>
</div>
</div> <!-- .table.header_main ends -->
</div> <!-- .container ends -->
</div> <!-- .sticky ends -->
</div> <!-- .header_wrapper ends -->";
    }

    public function getTemplateName()
    {
        return "basel/template/common/headers/header1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  263 => 101,  254 => 97,  249 => 95,  241 => 90,  237 => 89,  234 => 88,  224 => 81,  214 => 73,  211 => 72,  207 => 70,  197 => 68,  190 => 66,  183 => 65,  181 => 64,  178 => 63,  176 => 62,  171 => 59,  159 => 56,  156 => 55,  154 => 54,  150 => 52,  145 => 49,  142 => 48,  128 => 47,  125 => 46,  108 => 45,  105 => 44,  103 => 43,  99 => 42,  96 => 41,  94 => 40,  90 => 38,  88 => 37,  82 => 34,  78 => 32,  70 => 26,  62 => 21,  58 => 20,  52 => 17,  48 => 15,  46 => 14,  43 => 13,  41 => 12,  33 => 7,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div class="header-wrapper header1 fixed-header-possible">*/
/* {% if top_line_style %}*/
/* <div class="top_line">*/
/*   <div class="container {{ top_line_width }}">*/
/*   	<div class="table">*/
/*         <div class="table-cell left sm-text-center xs-text-center">*/
/*             <div class="promo-message">{{ promo_message }}</div>*/
/*         </div>*/
/*         <div class="table-cell text-right hidden-xs hidden-sm">*/
/*             <div class="links">*/
/*             <ul>*/
/*             {% include 'basel/template/common/static_links.twig' %}*/
/*             </ul>*/
/*             {% if lang_curr_title %}*/
/*             <div class="setting-ul">*/
/*             <div class="setting-li dropdown-wrapper from-left lang-curr-trigger nowrap"><a>*/
/*             <span>{{ lang_curr_title }}</span>*/
/*             </a>*/
/*             <div class="dropdown-content dropdown-right lang-curr-wrapper">*/
/*             {{ language }}*/
/*             {{ currency }}*/
/*             </div>*/
/*             </div>*/
/*             </div>*/
/*             {% endif %}*/
/*             </div>*/
/*         </div>*/
/*     </div> <!-- .table ends -->*/
/*   </div> <!-- .container ends -->*/
/* </div> <!-- .top_line ends -->*/
/* {% endif %}*/
/* <span class="table header-main sticky-header-placeholder">&nbsp;</span>*/
/* <div class="sticky-header outer-container header-style">*/
/* <div class="container {{ main_header_width }}">*/
/* <div class="table header-main">*/
/* <div class="table-cell w40 menu-cell hidden-xs hidden-sm">*/
/* {% if primary_menu %}*/
/* <div class="main-menu menu-stay-left">*/
/* 	<ul class="categories">*/
/* 		{% if primary_menu == 'oc' %}*/
/*         <!-- Default menu -->*/
/*         {{ default_menu }}*/
/*       {% elseif primary_menu is defined %}*/
/*         <!-- Mega menu -->*/
/*         {% for key, row in primary_menu_desktop %}*/
/*         {% include 'basel/template/common/menus/mega_menu.twig' %}*/
/*         {% endfor %}*/
/*       {% endif %}*/
/*     </ul>*/
/* </div>*/
/* {% endif %}*/
/* </div>*/
/* <div class="table-cell w20 logo text-center">*/
/*     {% if logo %}*/
/*     <div id="logo">*/
/*     <a href="{{ home }}"><img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" /></a>*/
/*     </div>*/
/*     {% endif %}*/
/* </div>*/
/* <div class="table-cell w40 shortcuts text-right"> */
/*    <div class="font-zero">*/
/*     {% if header_login %}*/
/*         <div class="shortcut-wrapper sign-in hidden-sx hidden-sm hidden-xs">*/
/*         {% if logged %}*/
/*         <a class="anim-underline" href="{{ account }}">{{ text_account }}</a> &nbsp;/&nbsp; */
/*         <a class="anim-underline" href="{{ logout }}">{{ text_logout }}</a>*/
/*         {% else %}*/
/*         <a class="anim-underline" href="{{ login }}">{{ text_login }} / {{ text_register }}</a>*/
/*         {% endif %}*/
/*         </div>*/
/*     {% endif %}*/
/*     {% if header_search %}*/
/*     <div class="icon-element">*/
/*     <div class="dropdown-wrapper-click from-top hidden-sx hidden-sm hidden-xs">*/
/*     <a class="shortcut-wrapper search-trigger from-top clicker">*/
/*     <i class="icon-magnifier icon"></i>*/
/*     </a>*/
/*     <div class="dropdown-content dropdown-right">*/
/*     <div class="search-dropdown-holder">*/
/*     <div class="search-holder">*/
/*     {{ basel_search }}*/
/*     </div>*/
/*     </div>*/
/*     </div>*/
/*     </div>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="icon-element is_wishlist">*/
/*     <a class="shortcut-wrapper wishlist" href="{{ wishlist }}">*/
/*     <div class="wishlist-hover"><i class="icon-heart icon"></i><span class="counter wishlist-counter">{{ wishlist_counter }}</span></div>*/
/*     </a>*/
/*     </div>*/
/*     <div class="icon-element catalog_hide">*/
/*     <div id="cart" class="dropdown-wrapper from-top">*/
/*     <a href="{{ shopping_cart }}" class="shortcut-wrapper cart">*/
/*     <i id="cart-icon" class="global-cart icon"></i> <span id="cart-total" class="nowrap">*/
/*     <span class="counter cart-total-items">{{ cart_items }}</span> <span class="slash hidden-md hidden-sm hidden-xs">/</span>&nbsp;<b class="cart-total-amount hidden-sm hidden-xs">{{ cart_amount }}</b>*/
/*     </span>*/
/*     </a>*/
/*     <div class="dropdown-content dropdown-right hidden-sm hidden-xs">*/
/*     {{ cart }}*/
/*     </div>*/
/*     </div>*/
/*     </div>*/
/*     <div class="icon-element">*/
/*     <a class="shortcut-wrapper menu-trigger hidden-md hidden-lg">*/
/*     <i class="icon-line-menu icon"></i>*/
/*     </a>*/
/*     </div>*/
/*    </div>*/
/* </div>*/
/* </div> <!-- .table.header_main ends -->*/
/* </div> <!-- .container ends -->*/
/* </div> <!-- .sticky ends -->*/
/* </div> <!-- .header_wrapper ends -->*/
