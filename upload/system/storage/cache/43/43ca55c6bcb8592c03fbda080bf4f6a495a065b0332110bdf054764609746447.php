<?php

/* extension/basel/panel_tabs/status.twig */
class __TwigTemplate_fabfe421a4534c2d7b6ce88793a17c662de5f3ff5b72bf70fff020e70c1f79d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Status</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Main setting to Enable/Disable Basel theme.\">Theme Status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[theme_default][theme_default_directory]\" value=\"default\" ";
        // line 5
        if (((isset($context["theme_default_directory"]) ? $context["theme_default_directory"] : null) == "default")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[theme_default][theme_default_directory]\" value=\"basel\" ";
        // line 6
        if (((isset($context["theme_default_directory"]) ? $context["theme_default_directory"] : null) == "basel")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
<input type=\"hidden\" name=\"settings[config][config_theme]\" value=\"default\" />
<input type=\"hidden\" name=\"settings[basel_version][basel_theme_version]\" value=\"1.2.9.0\" />
</div>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/status.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 6,  25 => 5,  19 => 1,);
    }
}
/* <legend>Status</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Main setting to Enable/Disable Basel theme.">Theme Status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[theme_default][theme_default_directory]" value="default" {% if theme_default_directory == 'default' %}checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[theme_default][theme_default_directory]" value="basel" {% if theme_default_directory == 'basel' %}checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* <input type="hidden" name="settings[config][config_theme]" value="default" />*/
/* <input type="hidden" name="settings[basel_version][basel_theme_version]" value="1.2.9.0" />*/
/* </div>*/
