<?php

/* extension/basel/panel_tabs/category-pages.twig */
class __TwigTemplate_5c2abe03c8905c91b78fbc25045f7943029e9c7d5b3283f5210e903f14e56f60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Category Pages</legend>

<legend class=\"sub\">Category Description</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable/Disable to include the category thumb in the category description.\">Category thumb</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][category_thumb_status]\" value=\"0\" ";
        // line 7
        if (((isset($context["category_thumb_status"]) ? $context["category_thumb_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][category_thumb_status]\" value=\"1\" ";
        // line 8
        if (((isset($context["category_thumb_status"]) ? $context["category_thumb_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<legend class=\"sub\">Sub Categories On Category Pages</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Show sub categories</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][category_subs_status]\" value=\"0\" ";
        // line 16
        if (((isset($context["category_subs_status"]) ? $context["category_subs_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][category_subs_status]\" value=\"1\" ";
        // line 17
        if (((isset($context["category_subs_status"]) ? $context["category_subs_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Sub categories per row</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_subs_grid]\" class=\"form-control\">
        <option value=\"2\"";
        // line 25
        if (((isset($context["basel_subs_grid"]) ? $context["basel_subs_grid"] : null) == "2")) {
            echo " selected=\"selected\"";
        }
        echo ">2</option>
        <option value=\"3\"";
        // line 26
        if (((isset($context["basel_subs_grid"]) ? $context["basel_subs_grid"] : null) == "3")) {
            echo " selected=\"selected\"";
        }
        echo ">3</option>
        <option value=\"4\"";
        // line 27
        if (((isset($context["basel_subs_grid"]) ? $context["basel_subs_grid"] : null) == "4")) {
            echo " selected=\"selected\"";
        }
        echo ">4</option>
        <option value=\"5\"";
        // line 28
        if (((isset($context["basel_subs_grid"]) ? $context["basel_subs_grid"] : null) == "5")) {
            echo " selected=\"selected\"";
        }
        echo ">5</option>
        <option value=\"6\"";
        // line 29
        if (((isset($context["basel_subs_grid"]) ? $context["basel_subs_grid"] : null) == "6")) {
            echo " selected=\"selected\"";
        }
        echo ">6</option>
    </select>
</div>
</div>

<legend class=\"sub\">Product Listings</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Number of products per row will also apply on search result pages, specials page and manufacturer pages\">Products per row</span></label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_prod_grid]\" class=\"form-control\">
        <option value=\"2\"";
        // line 39
        if (((isset($context["basel_prod_grid"]) ? $context["basel_prod_grid"] : null) == "2")) {
            echo " selected=\"selected\"";
        }
        echo ">2</option>
        <option value=\"3\"";
        // line 40
        if (((isset($context["basel_prod_grid"]) ? $context["basel_prod_grid"] : null) == "3")) {
            echo " selected=\"selected\"";
        }
        echo ">3</option>
        <option value=\"4\"";
        // line 41
        if (((isset($context["basel_prod_grid"]) ? $context["basel_prod_grid"] : null) == "4")) {
            echo " selected=\"selected\"";
        }
        echo ">4</option>
        <option value=\"5\"";
        // line 42
        if (((isset($context["basel_prod_grid"]) ? $context["basel_prod_grid"] : null) == "5")) {
            echo " selected=\"selected\"";
        }
        echo ">5</option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Products per page</label>
    <div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[theme_default][theme_default_product_limit]\" value=\"";
        // line 50
        echo (((isset($context["theme_default_product_limit"]) ? $context["theme_default_product_limit"] : null)) ? ((isset($context["theme_default_product_limit"]) ? $context["theme_default_product_limit"] : null)) : ("12"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"In the list view, short description character limit (categories, special etc)\">List Description Limit</span></label>
    <div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[theme_default][theme_default_product_description_length]\" value=\"";
        // line 57
        echo (((isset($context["theme_default_product_description_length"]) ? $context["theme_default_product_description_length"] : null)) ? ((isset($context["theme_default_product_description_length"]) ? $context["theme_default_product_description_length"] : null)) : ("190"));
        echo "\" />
    </div>                   
</div>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/category-pages.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 57,  135 => 50,  122 => 42,  116 => 41,  110 => 40,  104 => 39,  89 => 29,  83 => 28,  77 => 27,  71 => 26,  65 => 25,  52 => 17,  46 => 16,  33 => 8,  27 => 7,  19 => 1,);
    }
}
/* <legend>Category Pages</legend>*/
/* */
/* <legend class="sub">Category Description</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable/Disable to include the category thumb in the category description.">Category thumb</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][category_thumb_status]" value="0" {% if category_thumb_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][category_thumb_status]" value="1" {% if category_thumb_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Sub Categories On Category Pages</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Show sub categories</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][category_subs_status]" value="0" {% if category_subs_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][category_subs_status]" value="1" {% if category_subs_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Sub categories per row</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_subs_grid]" class="form-control">*/
/*         <option value="2"{% if basel_subs_grid == '2' %} selected="selected"{% endif %}>2</option>*/
/*         <option value="3"{% if basel_subs_grid == '3' %} selected="selected"{% endif %}>3</option>*/
/*         <option value="4"{% if basel_subs_grid == '4' %} selected="selected"{% endif %}>4</option>*/
/*         <option value="5"{% if basel_subs_grid == '5' %} selected="selected"{% endif %}>5</option>*/
/*         <option value="6"{% if basel_subs_grid == '6' %} selected="selected"{% endif %}>6</option>*/
/*     </select>*/
/* </div>*/
/* </div>*/
/* */
/* <legend class="sub">Product Listings</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Number of products per row will also apply on search result pages, specials page and manufacturer pages">Products per row</span></label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_prod_grid]" class="form-control">*/
/*         <option value="2"{% if basel_prod_grid == '2' %} selected="selected"{% endif %}>2</option>*/
/*         <option value="3"{% if basel_prod_grid == '3' %} selected="selected"{% endif %}>3</option>*/
/*         <option value="4"{% if basel_prod_grid == '4' %} selected="selected"{% endif %}>4</option>*/
/*         <option value="5"{% if basel_prod_grid == '5' %} selected="selected"{% endif %}>5</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Products per page</label>*/
/*     <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[theme_default][theme_default_product_limit]" value="{{ theme_default_product_limit ? theme_default_product_limit : '12' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="In the list view, short description character limit (categories, special etc)">List Description Limit</span></label>*/
/*     <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[theme_default][theme_default_product_description_length]" value="{{ theme_default_product_description_length ? theme_default_product_description_length : '190' }}" />*/
/*     </div>                   */
/* </div>*/
