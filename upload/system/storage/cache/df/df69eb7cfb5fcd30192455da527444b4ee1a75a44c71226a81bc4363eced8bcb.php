<?php

/* basel/template/common/currency.twig */
class __TwigTemplate_fa4ddbbd8b8e14e0177d3773253f0c91bd64ccd69b35b06ee5da41f720385dbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["currencies"]) ? $context["currencies"] : null)) > 1)) {
            // line 2
            echo "<div class=\"option\">
<h4>";
            // line 3
            echo (isset($context["text_currency"]) ? $context["text_currency"] : null);
            echo "</h4>
";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 5
                if (($this->getAttribute($context["currency"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                    // line 6
                    echo "<p><span class=\"anim-underline active\">";
                    echo $this->getAttribute($context["currency"], "title", array());
                    echo "</span></p>
";
                } else {
                    // line 8
                    echo "<p><a class=\"anim-underline\" onclick=\"\$('input[name=\\'code\\']').attr('value', '";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "'); \$('#form-currency').submit();\">";
                    echo $this->getAttribute($context["currency"], "title", array());
                    echo "</a></p>
";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 11
            echo "<select name=\"currency-select\" id=\"currency-select\">
";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 13
                if (($this->getAttribute($context["currency"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                    // line 14
                    echo "<option value=\"";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "</option>
";
                } else {
                    // line 16
                    echo "<option value=\"";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "\">";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "</option>
";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "</select>
<form action=\"";
            // line 20
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-currency\">  
<input type=\"hidden\" name=\"code\" id=\"curr-code\" value=\"\" />
<input type=\"hidden\" name=\"redirect\" value=\"";
            // line 22
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "\" />
</form>
<script>
\$(document).ready(function() {
\$('.mobile-lang-curr').addClass('has-c');
\$('#currency-select').appendTo('.mobile-lang-curr');
});
</script>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/common/currency.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 22,  84 => 20,  81 => 19,  69 => 16,  61 => 14,  59 => 13,  55 => 12,  52 => 11,  40 => 8,  34 => 6,  32 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if currencies|length > 1 %}*/
/* <div class="option">*/
/* <h4>{{ text_currency }}</h4>*/
/* {% for currency in currencies %}*/
/* {% if currency.code == code %}*/
/* <p><span class="anim-underline active">{{ currency.title }}</span></p>*/
/* {% else %}*/
/* <p><a class="anim-underline" onclick="$('input[name=\'code\']').attr('value', '{{ currency.code }}'); $('#form-currency').submit();">{{ currency.title }}</a></p>*/
/* {% endif %}*/
/* {% endfor %}*/
/* <select name="currency-select" id="currency-select">*/
/* {% for currency in currencies %}*/
/* {% if currency.code == code %}*/
/* <option value="{{ currency.code }}" selected="selected">{{ currency.code }}</option>*/
/* {% else %}*/
/* <option value="{{ currency.code }}">{{ currency.code }}</option>*/
/* {% endif %}*/
/* {% endfor %}*/
/* </select>*/
/* <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-currency">  */
/* <input type="hidden" name="code" id="curr-code" value="" />*/
/* <input type="hidden" name="redirect" value="{{ redirect }}" />*/
/* </form>*/
/* <script>*/
/* $(document).ready(function() {*/
/* $('.mobile-lang-curr').addClass('has-c');*/
/* $('#currency-select').appendTo('.mobile-lang-curr');*/
/* });*/
/* </script>*/
/* </div>*/
/* {% endif %}*/
