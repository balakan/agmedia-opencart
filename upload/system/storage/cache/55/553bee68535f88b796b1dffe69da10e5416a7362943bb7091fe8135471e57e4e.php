<?php

/* basel/template/common/footer.twig */
class __TwigTemplate_340a8b8acffbd362b9d40c9b9d8acb2675301a53552ea6a9048843f6c17373a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
";
        // line 2
        echo (isset($context["position_bottom_half"]) ? $context["position_bottom_half"] : null);
        echo "
</div>
<div class=\"container\">
";
        // line 5
        echo (isset($context["position_bottom"]) ? $context["position_bottom"] : null);
        echo "
</div>
<div id=\"footer\">
<div class=\"container\">
";
        // line 9
        if (((isset($context["footer_block_1"]) ? $context["footer_block_1"] : null) && ((isset($context["footer_block_1"]) ? $context["footer_block_1"] : null) != "<p><br></p>"))) {
            // line 10
            echo "<div class=\"footer-top-block\">
";
            // line 11
            echo (isset($context["footer_block_1"]) ? $context["footer_block_1"] : null);
            echo "
</div>
";
        }
        // line 14
        echo "<div class=\"row links-holder\">
<div class=\"col-xs-12 col-sm-8\">
  <div class=\"row\">
  ";
        // line 17
        if ((isset($context["custom_links"]) ? $context["custom_links"] : null)) {
            // line 18
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["basel_footer_columns"]) ? $context["basel_footer_columns"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                // line 19
                echo "    <div class=\"footer-column col-xs-12 col-sm-6 ";
                echo (isset($context["basel_columns_count"]) ? $context["basel_columns_count"] : null);
                echo " eq_height\">
      ";
                // line 20
                if ($this->getAttribute($context["column"], "title", array())) {
                    // line 21
                    echo "        <h5>";
                    echo $this->getAttribute($context["column"], "title", array());
                    echo "</h5>
      ";
                }
                // line 23
                echo "      ";
                if ($this->getAttribute($context["column"], "links", array(), "any", true, true)) {
                    // line 24
                    echo "      <ul class=\"list-unstyled\">
      ";
                    // line 25
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["column"], "links", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                        // line 26
                        echo "      <li><a href=\"";
                        echo $this->getAttribute($context["link"], "target", array());
                        echo "\">";
                        echo $this->getAttribute($context["link"], "title", array());
                        echo "</a></li>
      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 28
                    echo "      </ul>
      ";
                }
                // line 30
                echo "    </div>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "  ";
        } else {
            // line 33
            echo "      ";
            if ((isset($context["informations"]) ? $context["informations"] : null)) {
                // line 34
                echo "      <div class=\"footer-column col-xs-12 col-sm-4 eq_height\">
        <h5>";
                // line 35
                echo (isset($context["text_information"]) ? $context["text_information"] : null);
                echo "</h5>
        <ul class=\"list-unstyled\">
          ";
                // line 37
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                    // line 38
                    echo "          <li><a href=\"";
                    echo $this->getAttribute($context["information"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["information"], "title", array());
                    echo "</a></li>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 40
                echo "          <li><a href=\"";
                echo (isset($context["contact"]) ? $context["contact"] : null);
                echo "\">";
                echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
                echo "</a></li>
        </ul>
      </div>
      ";
            }
            // line 44
            echo "      <div class=\"footer-column col-xs-12 col-sm-4 eq_height\">
        <h5>";
            // line 45
            echo (isset($context["text_extra"]) ? $context["text_extra"] : null);
            echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
            // line 47
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo "\">";
            echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 48
            echo (isset($context["voucher"]) ? $context["voucher"] : null);
            echo "\">";
            echo (isset($context["text_voucher"]) ? $context["text_voucher"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 49
            echo (isset($context["affiliate"]) ? $context["affiliate"] : null);
            echo "\">";
            echo (isset($context["text_affiliate"]) ? $context["text_affiliate"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 50
            echo (isset($context["special"]) ? $context["special"] : null);
            echo "\">";
            echo (isset($context["text_special"]) ? $context["text_special"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 51
            echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
            echo "\">";
            echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
            echo "</a></li>
        </ul>
      </div>
      <div class=\"footer-column col-xs-12 col-sm-4 eq_height\">
        <h5>";
            // line 55
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</h5>
        <ul class=\"list-unstyled\">
          <li><a href=\"";
            // line 57
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 58
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 59
            echo (isset($context["return"]) ? $context["return"] : null);
            echo "\">";
            echo (isset($context["text_return"]) ? $context["text_return"] : null);
            echo "</a></li>
          <li class=\"is_wishlist\"><a href=\"";
            // line 60
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\">";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a></li>
          <li><a href=\"";
            // line 61
            echo (isset($context["newsletter"]) ? $context["newsletter"] : null);
            echo "\">";
            echo (isset($context["text_newsletter"]) ? $context["text_newsletter"] : null);
            echo "</a></li>
        </ul>
      </div>
 ";
        }
        // line 65
        echo "</div><!-- .row ends -->
</div><!-- .col-md-8 ends -->
<div class=\"footer-column col-xs-12 col-sm-4\">
<div class=\"footer-custom-wrapper\">
";
        // line 69
        if (array_key_exists("footer_block_title", $context)) {
            // line 70
            echo "<h5>";
            echo (isset($context["footer_block_title"]) ? $context["footer_block_title"] : null);
            echo "</h5>
";
        }
        // line 72
        if (((isset($context["footer_block_2"]) ? $context["footer_block_2"] : null) && ((isset($context["footer_block_2"]) ? $context["footer_block_2"] : null) != "<p><br></p>"))) {
            // line 73
            echo "<div class=\"custom_block\">";
            echo (isset($context["footer_block_2"]) ? $context["footer_block_2"] : null);
            echo "</div>
";
        }
        // line 75
        if (array_key_exists("footer_infoline_1", $context)) {
            // line 76
            echo "<p class=\"infoline\">";
            echo (isset($context["footer_infoline_1"]) ? $context["footer_infoline_1"] : null);
            echo "</p>
";
        }
        // line 78
        if (array_key_exists("footer_infoline_2", $context)) {
            // line 79
            echo "<p class=\"infoline\">";
            echo (isset($context["footer_infoline_2"]) ? $context["footer_infoline_2"] : null);
            echo "</p>
";
        }
        // line 81
        if (array_key_exists("footer_infoline_3", $context)) {
            // line 82
            echo "<p class=\"infoline\">";
            echo (isset($context["footer_infoline_3"]) ? $context["footer_infoline_3"] : null);
            echo "</p>
";
        }
        // line 84
        if ((isset($context["payment_img"]) ? $context["payment_img"] : null)) {
            // line 85
            echo "<img class=\"payment_img\" src=\"";
            echo (isset($context["payment_img"]) ? $context["payment_img"] : null);
            echo "\" alt=\"\" />
";
        }
        // line 87
        echo "</div>
</div>
</div><!-- .row ends -->
";
        // line 90
        if ((isset($context["basel_copyright"]) ? $context["basel_copyright"] : null)) {
            // line 91
            echo "<div class=\"footer-copyright\">";
            echo (isset($context["basel_copyright"]) ? $context["basel_copyright"] : null);
            echo "</div>
";
        }
        // line 93
        echo "</div>
</div>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />
<link href=\"catalog/view/theme/basel/js/lightgallery/css/lightgallery.css\" rel=\"stylesheet\" />
<script src=\"catalog/view/theme/basel/js/jquery.matchHeight.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/countdown.js\"></script>
<script src=\"catalog/view/theme/basel/js/live_search.js\"></script>
<script src=\"catalog/view/theme/basel/js/featherlight.js\"></script>
";
        // line 101
        if ((isset($context["view_popup"]) ? $context["view_popup"] : null)) {
            // line 102
            echo "<!-- Popup -->
<script>
\$(document).ready(function() {
if (\$(window).width() > ";
            // line 105
            echo (isset($context["popup_width_limit"]) ? $context["popup_width_limit"] : null);
            echo ") {
setTimeout(function() {
\$.featherlight({ajax: 'index.php?route=extension/basel/basel_features/basel_popup', variant:'popup-wrapper'});
}, ";
            // line 108
            echo (isset($context["popup_delay"]) ? $context["popup_delay"] : null);
            echo ");
}
});
</script>
";
        }
        // line 113
        if ((isset($context["sticky_columns"]) ? $context["sticky_columns"] : null)) {
            // line 114
            echo "<!-- Sticky columns -->
<script>
if (\$(window).width() > 767) {
\$('#column-left, #column-right').theiaStickySidebar({containerSelector:\$(this).closest('.row'),additionalMarginTop:";
            // line 117
            echo (isset($context["sticky_columns_offset"]) ? $context["sticky_columns_offset"] : null);
            echo "});
}
</script>
";
        }
        // line 121
        if ((isset($context["view_cookie_bar"]) ? $context["view_cookie_bar"] : null)) {
            // line 122
            echo "<!-- Cookie bar -->
<div class=\"basel_cookie_bar\">
<div class=\"table\">
<div class=\"table-cell w100\">";
            // line 125
            echo (isset($context["basel_cookie_info"]) ? $context["basel_cookie_info"] : null);
            echo "</div>
<div class=\"table-cell button-cell\">
<a class=\"btn btn-tiny btn-light-outline\" onclick=\"\$(this).parent().parent().parent().fadeOut(400);\">";
            // line 127
            echo (isset($context["basel_cookie_btn_close"]) ? $context["basel_cookie_btn_close"] : null);
            echo "</a>
";
            // line 128
            if (array_key_exists("href_more_info", $context)) {
                // line 129
                echo "<a class=\"more-info anim-underline light\" href=\"";
                echo (isset($context["href_more_info"]) ? $context["href_more_info"] : null);
                echo "\">";
                echo (isset($context["basel_cookie_btn_more_info"]) ? $context["basel_cookie_btn_more_info"] : null);
                echo "</a>
";
            }
            // line 131
            echo "</div>
</div>
</div>
";
        }
        // line 135
        echo "<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
BASEL VERSION ";
        // line 138
        echo (isset($context["basel_version"]) ? $context["basel_version"] : null);
        echo " - OPENCART VERSION 3 (";
        echo (isset($context["VERSION"]) ? $context["VERSION"] : null);
        echo ") 
//-->
</div><!-- .outer-container ends -->
<a class=\"scroll-to-top primary-bg-color hidden-sm hidden-xs\" onclick=\"\$('html, body').animate({scrollTop:0});\"><i class=\"icon-arrow-right\"></i></a>
<div id=\"featherlight-holder\"></div>
</body></html>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  370 => 138,  365 => 135,  359 => 131,  351 => 129,  349 => 128,  345 => 127,  340 => 125,  335 => 122,  333 => 121,  326 => 117,  321 => 114,  319 => 113,  311 => 108,  305 => 105,  300 => 102,  298 => 101,  288 => 93,  282 => 91,  280 => 90,  275 => 87,  269 => 85,  267 => 84,  261 => 82,  259 => 81,  253 => 79,  251 => 78,  245 => 76,  243 => 75,  237 => 73,  235 => 72,  229 => 70,  227 => 69,  221 => 65,  212 => 61,  206 => 60,  200 => 59,  194 => 58,  188 => 57,  183 => 55,  174 => 51,  168 => 50,  162 => 49,  156 => 48,  150 => 47,  145 => 45,  142 => 44,  132 => 40,  121 => 38,  117 => 37,  112 => 35,  109 => 34,  106 => 33,  103 => 32,  96 => 30,  92 => 28,  81 => 26,  77 => 25,  74 => 24,  71 => 23,  65 => 21,  63 => 20,  58 => 19,  53 => 18,  51 => 17,  46 => 14,  40 => 11,  37 => 10,  35 => 9,  28 => 5,  22 => 2,  19 => 1,);
    }
}
/* <div class="container">*/
/* {{ position_bottom_half }}*/
/* </div>*/
/* <div class="container">*/
/* {{ position_bottom }}*/
/* </div>*/
/* <div id="footer">*/
/* <div class="container">*/
/* {% if footer_block_1 and footer_block_1 != '<p><br></p>' %}*/
/* <div class="footer-top-block">*/
/* {{ footer_block_1 }}*/
/* </div>*/
/* {% endif %}*/
/* <div class="row links-holder">*/
/* <div class="col-xs-12 col-sm-8">*/
/*   <div class="row">*/
/*   {% if custom_links %}*/
/*     {% for column in basel_footer_columns %}*/
/*     <div class="footer-column col-xs-12 col-sm-6 {{ basel_columns_count }} eq_height">*/
/*       {% if column.title %}*/
/*         <h5>{{ column.title }}</h5>*/
/*       {% endif %}*/
/*       {% if column.links is defined %}*/
/*       <ul class="list-unstyled">*/
/*       {% for link in column.links %}*/
/*       <li><a href="{{ link.target }}">{{ link.title }}</a></li>*/
/*       {% endfor %}*/
/*       </ul>*/
/*       {% endif %}*/
/*     </div>*/
/*     {% endfor %}*/
/*   {% else %}*/
/*       {% if informations %}*/
/*       <div class="footer-column col-xs-12 col-sm-4 eq_height">*/
/*         <h5>{{ text_information }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           {% for information in informations %}*/
/*           <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*           {% endfor %}*/
/*           <li><a href="{{ contact }}">{{ text_contact }}</a></li>*/
/*         </ul>*/
/*       </div>*/
/*       {% endif %}*/
/*       <div class="footer-column col-xs-12 col-sm-4 eq_height">*/
/*         <h5>{{ text_extra }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           <li><a href="{{ manufacturer }}">{{ text_manufacturer }}</a></li>*/
/*           <li><a href="{{ voucher }}">{{ text_voucher }}</a></li>*/
/*           <li><a href="{{ affiliate }}">{{ text_affiliate }}</a></li>*/
/*           <li><a href="{{ special }}">{{ text_special }}</a></li>*/
/*           <li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>*/
/*         </ul>*/
/*       </div>*/
/*       <div class="footer-column col-xs-12 col-sm-4 eq_height">*/
/*         <h5>{{ text_account }}</h5>*/
/*         <ul class="list-unstyled">*/
/*           <li><a href="{{ account }}">{{ text_account }}</a></li>*/
/*           <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/*           <li><a href="{{ return }}">{{ text_return }}</a></li>*/
/*           <li class="is_wishlist"><a href="{{ wishlist }}">{{ text_wishlist }}</a></li>*/
/*           <li><a href="{{ newsletter }}">{{ text_newsletter }}</a></li>*/
/*         </ul>*/
/*       </div>*/
/*  {% endif %}*/
/* </div><!-- .row ends -->*/
/* </div><!-- .col-md-8 ends -->*/
/* <div class="footer-column col-xs-12 col-sm-4">*/
/* <div class="footer-custom-wrapper">*/
/* {% if footer_block_title is defined %}*/
/* <h5>{{ footer_block_title }}</h5>*/
/* {% endif %}*/
/* {% if footer_block_2 and footer_block_2 != '<p><br></p>' %}*/
/* <div class="custom_block">{{ footer_block_2 }}</div>*/
/* {% endif %}*/
/* {% if footer_infoline_1 is defined %}*/
/* <p class="infoline">{{ footer_infoline_1 }}</p>*/
/* {% endif %}*/
/* {% if footer_infoline_2 is defined %}*/
/* <p class="infoline">{{ footer_infoline_2 }}</p>*/
/* {% endif %}*/
/* {% if footer_infoline_3 is defined %}*/
/* <p class="infoline">{{ footer_infoline_3 }}</p>*/
/* {% endif %}*/
/* {% if payment_img %}*/
/* <img class="payment_img" src="{{ payment_img }}" alt="" />*/
/* {% endif %}*/
/* </div>*/
/* </div>*/
/* </div><!-- .row ends -->*/
/* {% if basel_copyright %}*/
/* <div class="footer-copyright">{{ basel_copyright }}</div>*/
/* {% endif %}*/
/* </div>*/
/* </div>*/
/* <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" />*/
/* <link href="catalog/view/theme/basel/js/lightgallery/css/lightgallery.css" rel="stylesheet" />*/
/* <script src="catalog/view/theme/basel/js/jquery.matchHeight.min.js"></script>*/
/* <script src="catalog/view/theme/basel/js/countdown.js"></script>*/
/* <script src="catalog/view/theme/basel/js/live_search.js"></script>*/
/* <script src="catalog/view/theme/basel/js/featherlight.js"></script>*/
/* {% if view_popup %}*/
/* <!-- Popup -->*/
/* <script>*/
/* $(document).ready(function() {*/
/* if ($(window).width() > {{ popup_width_limit }}) {*/
/* setTimeout(function() {*/
/* $.featherlight({ajax: 'index.php?route=extension/basel/basel_features/basel_popup', variant:'popup-wrapper'});*/
/* }, {{ popup_delay }});*/
/* }*/
/* });*/
/* </script>*/
/* {% endif %}*/
/* {% if sticky_columns %}*/
/* <!-- Sticky columns -->*/
/* <script>*/
/* if ($(window).width() > 767) {*/
/* $('#column-left, #column-right').theiaStickySidebar({containerSelector:$(this).closest('.row'),additionalMarginTop:{{ sticky_columns_offset }}});*/
/* }*/
/* </script>*/
/* {% endif %}*/
/* {% if view_cookie_bar %}*/
/* <!-- Cookie bar -->*/
/* <div class="basel_cookie_bar">*/
/* <div class="table">*/
/* <div class="table-cell w100">{{ basel_cookie_info }}</div>*/
/* <div class="table-cell button-cell">*/
/* <a class="btn btn-tiny btn-light-outline" onclick="$(this).parent().parent().parent().fadeOut(400);">{{ basel_cookie_btn_close }}</a>*/
/* {% if href_more_info is defined %}*/
/* <a class="more-info anim-underline light" href="{{ href_more_info }}">{{ basel_cookie_btn_more_info }}</a>*/
/* {% endif %}*/
/* </div>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* <!--*/
/* OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.*/
/* Please donate via PayPal to donate@opencart.com*/
/* BASEL VERSION {{ basel_version }} - OPENCART VERSION 3 ({{ VERSION }}) */
/* //-->*/
/* </div><!-- .outer-container ends -->*/
/* <a class="scroll-to-top primary-bg-color hidden-sm hidden-xs" onclick="$('html, body').animate({scrollTop:0});"><i class="icon-arrow-right"></i></a>*/
/* <div id="featherlight-holder"></div>*/
/* </body></html>*/
