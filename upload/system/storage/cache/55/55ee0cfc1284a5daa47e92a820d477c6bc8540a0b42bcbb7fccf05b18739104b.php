<?php

/* extension/basel/panel_tabs/notifications.twig */
class __TwigTemplate_22c4852c2b8b75d3c255a72c00fffa46446db0470f966ed3bc0eca4d1ebfe43d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Notifications</legend>

<legend class=\"sub\">Top Line Promotion Message</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"The top line notification can be used to put a text message to the top of the header\">Status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_status]\" value=\"0\" ";
        // line 8
        if (((isset($context["basel_top_promo_status"]) ? $context["basel_top_promo_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_status]\" value=\"1\" ";
        // line 9
        if (((isset($context["basel_top_promo_status"]) ? $context["basel_top_promo_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Width</label>
    <div class=\"col-sm-10 toggle-btn both-blue\">
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_width]\" value=\"\" ";
        // line 16
        if (((isset($context["basel_top_promo_width"]) ? $context["basel_top_promo_width"] : null) == "")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Boxed</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_width]\" value=\"full-width\" ";
        // line 17
        if (((isset($context["basel_top_promo_width"]) ? $context["basel_top_promo_width"] : null) == "full-width")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Full Width</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"If enabled, a close button will be added to the message, and if clicked, a cookie will  block the message for that user the next 30 days\">Close button</span></label>
    <div class=\"col-sm-10 toggle-btn both-blue\">
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_close]\" value=\"0\" ";
        // line 24
        if (((isset($context["basel_top_promo_close"]) ? $context["basel_top_promo_close"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_close]\" value=\"1\" ";
        // line 25
        if (((isset($context["basel_top_promo_close"]) ? $context["basel_top_promo_close"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Text alignment</label>
    <div class=\"col-sm-10 toggle-btn both-blue\">
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_align]\" value=\"\" ";
        // line 32
        if (((isset($context["basel_top_promo_align"]) ? $context["basel_top_promo_align"] : null) == "")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Left</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_top_promo_align]\" value=\"text-center\" ";
        // line 33
        if (((isset($context["basel_top_promo_align"]) ? $context["basel_top_promo_align"] : null) == "text-center")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Center</span></label>
    </div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Promo text</label>
    <div class=\"col-sm-10\">
    ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 41
            echo "    <div class=\"input-group\">
    <span class=\"input-group-addon\">
    <img src=\"language/";
            // line 43
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" /></span>
    <input class=\"form-control\" name=\"settings[basel][basel_top_promo_text][";
            // line 44
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["basel_top_promo_text"]) ? $context["basel_top_promo_text"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["basel_top_promo_text"]) ? $context["basel_top_promo_text"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "    </div>
</div>

<legend class=\"sub\">Cookie Information Bar</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Cookie bar status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_cookie_bar_status]\" value=\"0\" ";
        // line 54
        if (((isset($context["basel_cookie_bar_status"]) ? $context["basel_cookie_bar_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_cookie_bar_status]\" value=\"1\" ";
        // line 55
        if (((isset($context["basel_cookie_bar_status"]) ? $context["basel_cookie_bar_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"The link target for the Read more link. Leave empty to disable the link\">Read more link target</span></label>
    <div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[basel][basel_cookie_bar_url]\" value=\"";
        // line 62
        echo (((isset($context["basel_cookie_bar_url"]) ? $context["basel_cookie_bar_url"] : null)) ? ((isset($context["basel_cookie_bar_url"]) ? $context["basel_cookie_bar_url"] : null)) : (""));
        echo "\" />
    </div>                   
</div>


<legend class=\"sub\">Popup Notification</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Popup notification status</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_status]\" value=\"0\" ";
        // line 72
        if (((isset($context["basel_popup_note_status"]) ? $context["basel_popup_note_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_status]\" value=\"1\" ";
        // line 73
        if (((isset($context["basel_popup_note_status"]) ? $context["basel_popup_note_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"If enabled, a cookie will be set for 30 days to prevent the popup for being shown more than one time per visitor\">Show only once</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_once]\" value=\"0\" ";
        // line 80
        if (((isset($context["basel_popup_note_once"]) ? $context["basel_popup_note_once"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_once]\" value=\"1\" ";
        // line 81
        if (((isset($context["basel_popup_note_once"]) ? $context["basel_popup_note_once"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Apply to home page only</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_home]\" value=\"0\" ";
        // line 88
        if (((isset($context["basel_popup_note_home"]) ? $context["basel_popup_note_home"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_home]\" value=\"1\" ";
        // line 89
        if (((isset($context["basel_popup_note_home"]) ? $context["basel_popup_note_home"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Delay in milliseconds until the popup opens. For example, 5000 will open the popup after 5 seconds. Enter 0 to show the popup immediately on page load.\">Popup delay</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:120px\" name=\"settings[basel][basel_popup_note_delay]\" value=\"";
        // line 96
        echo (((isset($context["basel_popup_note_delay"]) ? $context["basel_popup_note_delay"] : null)) ? ((isset($context["basel_popup_note_delay"]) ? $context["basel_popup_note_delay"] : null)) : ("5000"));
        echo "\" /> ms
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Width * Height\">Popup size</span></label>
    <div class=\"col-sm-10 form-inline\">
    <input class=\"form-control\" style=\"width:120px\" name=\"settings[basel][basel_popup_note_w]\" value=\"";
        // line 103
        echo (((isset($context["basel_popup_note_w"]) ? $context["basel_popup_note_w"] : null)) ? ((isset($context["basel_popup_note_w"]) ? $context["basel_popup_note_w"] : null)) : ("860"));
        echo "\" /> px &nbsp;
    <input class=\"form-control\" style=\"width:120px\" name=\"settings[basel][basel_popup_note_h]\" value=\"";
        // line 104
        echo (((isset($context["basel_popup_note_h"]) ? $context["basel_popup_note_h"] : null)) ? ((isset($context["basel_popup_note_h"]) ? $context["basel_popup_note_h"] : null)) : ("500"));
        echo "\" /> px 
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable or disable the popup to visitors using a mobile device\">Mobile status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_m]\" value=\"767\" ";
        // line 111
        if (((isset($context["basel_popup_note_m"]) ? $context["basel_popup_note_m"] : null) == "767")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_popup_note_m]\" value=\"10\" ";
        // line 112
        if (((isset($context["basel_popup_note_m"]) ? $context["basel_popup_note_m"] : null) == "10")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"If an image is chosen, it will take place at the left side of the popup window\">Image</span></label>
    <div class=\"col-sm-10\">
    <a href=\"\" id=\"thumb-popup-img\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 119
        echo (isset($context["popup_thumb"]) ? $context["popup_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
    <input type=\"hidden\" name=\"settings[basel][basel_popup_note_img]\" value=\"";
        // line 120
        echo (isset($context["basel_popup_note_img"]) ? $context["basel_popup_note_img"] : null);
        echo "\" id=\"input-popup-img\" />
    </div>                   
</div>

<ul class=\"nav nav-tabs language-tabs\">
";
        // line 125
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 126
            echo "<li><a href=\"#basel_popup_note_block";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 128
        echo "</ul>
<div class=\"tab-content\">
";
        // line 130
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 131
            echo "<div class=\"tab-pane\" id=\"basel_popup_note_block";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Heading</label>
<div class=\"col-sm-10\">
  <input class=\"form-control\" name=\"settings[basel][basel_popup_note_title][";
            // line 135
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["basel_popup_note_title"]) ? $context["basel_popup_note_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["basel_popup_note_title"]) ? $context["basel_popup_note_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
</div>
</div>

  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use shortcode {signup} to add a newsletter signup field\">Popup content</span>
    <br /><a class=\"editor-toggle\" id=\"enable-editor-popup-1\" onclick=\"enable_editor('popup','1')\">Enable HTML editor</a></label>
    <div class=\"col-sm-10\">
      <textarea id=\"editor-textarea-popup-1\" name=\"settings[basel][basel_popup_note_block][";
            // line 143
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" class=\"form-control code\">";
            echo (($this->getAttribute((isset($context["basel_popup_note_block"]) ? $context["basel_popup_note_block"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["basel_popup_note_block"]) ? $context["basel_popup_note_block"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "</textarea>
    </div>
  </div>
  
 </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/notifications.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  341 => 149,  327 => 143,  314 => 135,  306 => 131,  302 => 130,  298 => 128,  281 => 126,  277 => 125,  269 => 120,  263 => 119,  251 => 112,  245 => 111,  235 => 104,  231 => 103,  221 => 96,  209 => 89,  203 => 88,  191 => 81,  185 => 80,  173 => 73,  167 => 72,  154 => 62,  142 => 55,  136 => 54,  127 => 47,  116 => 44,  108 => 43,  104 => 41,  100 => 40,  88 => 33,  82 => 32,  70 => 25,  64 => 24,  52 => 17,  46 => 16,  34 => 9,  28 => 8,  19 => 1,);
    }
}
/* <legend>Notifications</legend>*/
/* */
/* <legend class="sub">Top Line Promotion Message</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="The top line notification can be used to put a text message to the top of the header">Status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_status]" value="0" {% if basel_top_promo_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_status]" value="1" {% if basel_top_promo_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Width</label>*/
/*     <div class="col-sm-10 toggle-btn both-blue">*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_width]" value="" {% if basel_top_promo_width == '' %} checked="checked"{% endif %} /><span>Boxed</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_width]" value="full-width" {% if basel_top_promo_width == 'full-width' %} checked="checked"{% endif %} /><span>Full Width</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="If enabled, a close button will be added to the message, and if clicked, a cookie will  block the message for that user the next 30 days">Close button</span></label>*/
/*     <div class="col-sm-10 toggle-btn both-blue">*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_close]" value="0" {% if basel_top_promo_close == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_close]" value="1" {% if basel_top_promo_close == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Text alignment</label>*/
/*     <div class="col-sm-10 toggle-btn both-blue">*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_align]" value="" {% if basel_top_promo_align == '' %} checked="checked"{% endif %} /><span>Left</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_top_promo_align]" value="text-center" {% if basel_top_promo_align == 'text-center' %} checked="checked"{% endif %} /><span>Center</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Promo text</label>*/
/*     <div class="col-sm-10">*/
/*     {% for language in languages %}*/
/*     <div class="input-group">*/
/*     <span class="input-group-addon">*/
/*     <img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*     <input class="form-control" name="settings[basel][basel_top_promo_text][{{ language.language_id }}]" value="{{ basel_top_promo_text[language.language_id] ? basel_top_promo_text[language.language_id] }}" />*/
/*     </div>*/
/*     {% endfor %}*/
/*     </div>*/
/* </div>*/
/* */
/* <legend class="sub">Cookie Information Bar</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Cookie bar status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_cookie_bar_status]" value="0" {% if basel_cookie_bar_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_cookie_bar_status]" value="1" {% if basel_cookie_bar_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="The link target for the Read more link. Leave empty to disable the link">Read more link target</span></label>*/
/*     <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[basel][basel_cookie_bar_url]" value="{{ basel_cookie_bar_url ? basel_cookie_bar_url }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <legend class="sub">Popup Notification</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Popup notification status</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_status]" value="0" {% if basel_popup_note_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_status]" value="1" {% if basel_popup_note_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="If enabled, a cookie will be set for 30 days to prevent the popup for being shown more than one time per visitor">Show only once</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_once]" value="0" {% if basel_popup_note_once == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_once]" value="1" {% if basel_popup_note_once == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Apply to home page only</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_home]" value="0" {% if basel_popup_note_home == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_home]" value="1" {% if basel_popup_note_home == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Delay in milliseconds until the popup opens. For example, 5000 will open the popup after 5 seconds. Enter 0 to show the popup immediately on page load.">Popup delay</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:120px" name="settings[basel][basel_popup_note_delay]" value="{{ basel_popup_note_delay ? basel_popup_note_delay : '5000' }}" /> ms*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Width * Height">Popup size</span></label>*/
/*     <div class="col-sm-10 form-inline">*/
/*     <input class="form-control" style="width:120px" name="settings[basel][basel_popup_note_w]" value="{{ basel_popup_note_w ? basel_popup_note_w : '860' }}" /> px &nbsp;*/
/*     <input class="form-control" style="width:120px" name="settings[basel][basel_popup_note_h]" value="{{ basel_popup_note_h ? basel_popup_note_h : '500' }}" /> px */
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable or disable the popup to visitors using a mobile device">Mobile status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_m]" value="767" {% if basel_popup_note_m == '767' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_popup_note_m]" value="10" {% if basel_popup_note_m == '10' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="If an image is chosen, it will take place at the left side of the popup window">Image</span></label>*/
/*     <div class="col-sm-10">*/
/*     <a href="" id="thumb-popup-img" data-toggle="image" class="img-thumbnail"><img src="{{ popup_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*     <input type="hidden" name="settings[basel][basel_popup_note_img]" value="{{ basel_popup_note_img }}" id="input-popup-img" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <ul class="nav nav-tabs language-tabs">*/
/* {% for language in languages %}*/
/* <li><a href="#basel_popup_note_block{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /> {{ language.name }}</a></li>*/
/* {% endfor %}*/
/* </ul>*/
/* <div class="tab-content">*/
/* {% for language in languages %}*/
/* <div class="tab-pane" id="basel_popup_note_block{{ language.language_id }}">*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Heading</label>*/
/* <div class="col-sm-10">*/
/*   <input class="form-control" name="settings[basel][basel_popup_note_title][{{ language.language_id }}]" value="{{ basel_popup_note_title[language.language_id] ? basel_popup_note_title[language.language_id] }}" />*/
/* </div>*/
/* </div>*/
/* */
/*   <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use shortcode {signup} to add a newsletter signup field">Popup content</span>*/
/*     <br /><a class="editor-toggle" id="enable-editor-popup-1" onclick="enable_editor('popup','1')">Enable HTML editor</a></label>*/
/*     <div class="col-sm-10">*/
/*       <textarea id="editor-textarea-popup-1" name="settings[basel][basel_popup_note_block][{{ language.language_id }}]" class="form-control code">{{ basel_popup_note_block[language.language_id] ? basel_popup_note_block[language.language_id] }}</textarea>*/
/*     </div>*/
/*   </div>*/
/*   */
/*  </div>*/
/* {% endfor %}*/
/* </div>*/
