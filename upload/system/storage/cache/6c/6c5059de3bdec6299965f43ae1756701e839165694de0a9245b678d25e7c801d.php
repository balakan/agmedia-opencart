<?php

/* extension/basel/panel_tabs/footer.twig */
class __TwigTemplate_7a7ac6849753b52100206516730c48d810986e555f5b30864eebce0bc75a23bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Footer</legend>

<legend class=\"sub\">Footer Custom Block (top)</legend>
<ul class=\"nav nav-tabs language-tabs\">
";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 6
            echo "<li><a href=\"#footer-block1";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "</ul>
<div class=\"tab-content\">
";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 11
            echo "<div class=\"tab-pane\" id=\"footer-block1";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">
  <div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Custom block which will take place above the footer links\">Footer custom block (top)</span><br /><a class=\"editor-toggle\" id=\"enable-editor-footer1-";
            // line 13
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" onclick=\"enable_editor('footer1','";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "')\">Enable HTML editor</a></label>
    <div class=\"col-sm-10\">
      <textarea id=\"editor-textarea-footer1-";
            // line 15
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" name=\"settings[basel][footer_block_1][";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" class=\"form-control code\">";
            echo (($this->getAttribute((isset($context["footer_block_1"]) ? $context["footer_block_1"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["footer_block_1"]) ? $context["footer_block_1"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "</textarea>
    </div>
  </div>
 </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "</div>

          
<legend class=\"sub\">Footer Links</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable to use custom footer links instead of the default ones in Opencart\">Overwrite default links</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][overwrite_footer_links]\" class=\"footer-custom-links-select\" value=\"0\" ";
        // line 27
        if (((isset($context["overwrite_footer_links"]) ? $context["overwrite_footer_links"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][overwrite_footer_links]\" class=\"footer-custom-links-select\" value=\"1\" ";
        // line 28
        if (((isset($context["overwrite_footer_links"]) ? $context["overwrite_footer_links"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>


<div id=\"custom_footer_links_holder\"";
        // line 33
        if ((isset($context["overwrite_footer_links"]) ? $context["overwrite_footer_links"] : null)) {
            echo " style=\"display:block\"";
        } else {
            echo " style=\"display:none\"";
        }
        echo ">
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"></label>
<div class=\"col-sm-10\">
<ul class=\"nav content-tabs\" id=\"footer-column-tabs\">
    ";
        // line 38
        $context["footer_column_row"] = 1;
        // line 39
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["basel_footer_columns"]) ? $context["basel_footer_columns"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["basel_footer_column"]) {
            // line 40
            echo "    <li><a href=\"#footer-column-";
            echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
            echo "\" data-toggle=\"tab\"><i class=\"fa fa-minus-circle\" onclick=\"\$('a[href=\\'#footer-column-";
            echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
            echo "\\']').parent().remove(); \$('#footer-column-";
            echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
            echo "').remove(); \$('#footer-column-tabs a:first').tab('show');\"></i> Column ";
            echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
            echo "</a></li>
    ";
            // line 41
            $context["footer_column_row"] = ((isset($context["footer_column_row"]) ? $context["footer_column_row"] : null) + 1);
            // line 42
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_footer_column'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "    <li id=\"column-add\" style=\"cursor:pointer;\"><a onclick=\"addFooterColumn();\"><i class=\"fa fa-plus-circle\"></i> Add Column</a></li> 
</ul>
<div class=\"tab-content\" id=\"footer-columns-holder\">
";
        // line 46
        $context["footer_column_row"] = 1;
        // line 47
        if (array_key_exists("basel_footer_columns", $context)) {
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["basel_footer_columns"]) ? $context["basel_footer_columns"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["basel_footer_column"]) {
                // line 49
                echo "<div class=\"tab-pane\" id=\"footer-column-";
                echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                echo "\">
<div class=\"tab-content\">
<table class=\"table table-clean table-footer-column\">
<thead>
  <tr>
    <td width=\"96%\"><h4>Column Heading</h4></td>
    <td width=\"4%\">Sort Order</td>
  </tr>
</thead>
<tbody>
  <tr id=\"column-row";
                // line 59
                echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                echo "\">
    <td class=\"first\">
    ";
                // line 61
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                    // line 62
                    echo "    <div class=\"input-group\">
    <span class=\"input-group-addon\"><img src=\"language/";
                    // line 63
                    echo $this->getAttribute($context["language"], "code", array());
                    echo "/";
                    echo $this->getAttribute($context["language"], "code", array());
                    echo ".png\" title=\"";
                    echo $this->getAttribute($context["language"], "language_id", array());
                    echo "\" /></span>
    <input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_footer_columns][";
                    // line 64
                    echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                    echo "][title][";
                    echo $this->getAttribute($context["language"], "language_id", array());
                    echo "]\" value=\"";
                    echo (($this->getAttribute($this->getAttribute($context["basel_footer_column"], "title", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute($context["basel_footer_column"], "title", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
                    echo "\" />
    </div>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 67
                echo "    </td>
    <td class=\"text-right\">
    <input type=\"text\" class=\"form-control\" style=\"width:60px\" name=\"settings[basel][basel_footer_columns][";
                // line 69
                echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                echo "][sort]\" value=\"";
                echo (($this->getAttribute($context["basel_footer_column"], "sort", array())) ? ($this->getAttribute($context["basel_footer_column"], "sort", array())) : ("99"));
                echo "\" />
    </td>
  </tr>
</tbody>
</table>
<table id=\"links-holder-";
                // line 74
                echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                echo "\" class=\"table table-clean\">
    <thead>
      <tr>
        <td width=\"48%\"><h4>Column Links</h4></td>
        <td width=\"48%\"><span data-toggle=\"tooltip\" title=\"Include http:// when linking to external targets\">Link Target</span></td>
        <td width=\"4%\"><span style=\"white-space:nowrap\">Sort Order</span></td>
      </tr>
    </thead>
    <tbody>
    <tr></tr>
    ";
                // line 84
                $context["footer_link_row"] = 1;
                // line 85
                echo "\t";
                if ($this->getAttribute($context["basel_footer_column"], "links", array(), "any", true, true)) {
                    // line 86
                    echo "\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["basel_footer_column"], "links", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
                        // line 87
                        echo "    <tr id=\"footer-link-row-";
                        echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                        echo "-";
                        echo (isset($context["footer_link_row"]) ? $context["footer_link_row"] : null);
                        echo "\">
    <td class=\"first\">
    ";
                        // line 89
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                            // line 90
                            echo "    <div class=\"input-group\">
    <span class=\"input-group-addon\"><img src=\"language/";
                            // line 91
                            echo $this->getAttribute($context["language"], "code", array());
                            echo "/";
                            echo $this->getAttribute($context["language"], "code", array());
                            echo ".png\" title=\"";
                            echo $this->getAttribute($context["language"], "language_id", array());
                            echo "\" /></span>
    <input class=\"form-control\" name=\"settings[basel][basel_footer_columns][";
                            // line 92
                            echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                            echo "][links][";
                            echo (isset($context["footer_link_row"]) ? $context["footer_link_row"] : null);
                            echo "][title][";
                            echo $this->getAttribute($context["language"], "language_id", array());
                            echo "]\" value=\"";
                            echo (($this->getAttribute($this->getAttribute($context["link"], "title", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute($context["link"], "title", array()), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
                            echo "\" />
    </div>
    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 95
                        echo "    </td>
    <td class=\"first\">
    <input class=\"form-control\" name=\"settings[basel][basel_footer_columns][";
                        // line 97
                        echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                        echo "][links][";
                        echo (isset($context["footer_link_row"]) ? $context["footer_link_row"] : null);
                        echo "][target]\" value=\"";
                        echo (($this->getAttribute($context["link"], "target", array())) ? ($this->getAttribute($context["link"], "target", array())) : (""));
                        echo "\" />
    </td>
    <td class=\"first\">
    <input type=\"text\" class=\"form-control\" style=\"width:60px\" name=\"settings[basel][basel_footer_columns][";
                        // line 100
                        echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                        echo "][links][";
                        echo (isset($context["footer_link_row"]) ? $context["footer_link_row"] : null);
                        echo "][sort]\" value=\"";
                        echo (($this->getAttribute($context["link"], "sort", array())) ? ($this->getAttribute($context["link"], "sort", array())) : ("1"));
                        echo "\" />
    </td>
    <td class=\"text-right\">
    <button type=\"button\" onclick=\"\$('#footer-link-row-";
                        // line 103
                        echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                        echo "-";
                        echo (isset($context["footer_link_row"]) ? $context["footer_link_row"] : null);
                        echo "').remove();\" class=\"btn btn-danger\">Remove</button>
    </td>
    </tr>
";
                        // line 106
                        $context["footer_link_row"] = ((isset($context["footer_link_row"]) ? $context["footer_link_row"] : null) + 1);
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 107
                    echo " <!-- foreach groups ends -->
";
                }
                // line 109
                echo "</tbody>
    <tfoot>
      <tr>
        <td colspan=\"3\"></td>
        <td class=\"text-right\"><button type=\"button\" onclick=\"addFooterLink(";
                // line 113
                echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
                echo ");\" class=\"btn btn-primary\">Add Link</button></td>
      </tr>
    </tfoot>
  </table>
</div>
</div> <!-- #footer-columns--->
";
                // line 119
                $context["footer_column_row"] = ((isset($context["footer_column_row"]) ? $context["footer_column_row"] : null) + 1);
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['basel_footer_column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 122
        echo "</div>  <!-- #footer-columns-holder-->
</div>                
</div>
</div>

<legend class=\"sub\">Footer Custom Block (right)</legend>
<ul class=\"nav nav-tabs language-tabs\">
";
        // line 129
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 130
            echo "<li><a href=\"#footer-block2-language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "</ul>
<div class=\"tab-content\">
";
        // line 134
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 135
            echo "<div class=\"tab-pane\" id=\"footer-block2-language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Heading</label>
<div class=\"col-sm-10\">
  <input class=\"form-control\" name=\"settings[basel][footer_block_title][";
            // line 140
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["footer_block_title"]) ? $context["footer_block_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["footer_block_title"]) ? $context["footer_block_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
</div>
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Custom block which will take place as a column right by the footer links\">Footer custom block (top)</span><br /><a class=\"editor-toggle\" id=\"enable-editor-footer2-";
            // line 145
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" onclick=\"enable_editor('footer2','";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "')\">Enable HTML editor</a></label>
<div class=\"col-sm-10\">
  <textarea id=\"editor-textarea-footer2-";
            // line 147
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" name=\"settings[basel][footer_block_2][";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" class=\"form-control code\">";
            echo (($this->getAttribute((isset($context["footer_block_2"]) ? $context["footer_block_2"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["footer_block_2"]) ? $context["footer_block_2"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "</textarea>
</div>
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Contact detail lines will take place below the custom block\">Contact details</span></label>
<div class=\"col-sm-10\">
  <input class=\"form-control\" name=\"settings[basel][footer_infoline_1][";
            // line 154
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["footer_infoline_1"]) ? $context["footer_infoline_1"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["footer_infoline_1"]) ? $context["footer_infoline_1"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
  <input class=\"form-control\" name=\"settings[basel][footer_infoline_2][";
            // line 155
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["footer_infoline_2"]) ? $context["footer_infoline_2"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["footer_infoline_2"]) ? $context["footer_infoline_2"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
  <input class=\"form-control\" name=\"settings[basel][footer_infoline_3][";
            // line 156
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["footer_infoline_3"]) ? $context["footer_infoline_3"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["footer_infoline_3"]) ? $context["footer_infoline_3"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
</div>
</div>
  
 </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 162
        echo "</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"If an image is uploaded, it will take place below the contact details\">Payment icons</span></label>
    <div class=\"col-sm-10\">
    <a href=\"\" id=\"thumb-payment-img\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 167
        echo (isset($context["payment_thumb"]) ? $context["payment_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
    <input type=\"hidden\" name=\"settings[basel][basel_payment_img]\" value=\"";
        // line 168
        echo (isset($context["basel_payment_img"]) ? $context["basel_payment_img"] : null);
        echo "\" id=\"input-payment-img\" />
    </div>                   
</div>


<legend class=\"sub\">Footer Copyright Text</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Use {year} to add the current year\">Footer Copyright Text</span></label>
    <div class=\"col-sm-10\">
    ";
        // line 177
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 178
            echo "    <div class=\"input-group\">
    <span class=\"input-group-addon\">
    <img src=\"language/";
            // line 180
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" /></span>
    <input class=\"form-control\" name=\"settings[basel][basel_copyright][";
            // line 181
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["basel_copyright"]) ? $context["basel_copyright"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["basel_copyright"]) ? $context["basel_copyright"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" />
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 184
        echo "    </div>
</div>

<script type=\"text/javascript\"><!--
\$('#footer-column-tabs a:first').tab('show');
var footer_column_row = ";
        // line 189
        echo (isset($context["footer_column_row"]) ? $context["footer_column_row"] : null);
        echo ";
function addFooterColumn() {
\thtml = '<div class=\"tab-pane\" id=\"footer-column-' + footer_column_row + '\">';
\thtml += '<div class=\"tab-content\">';
\thtml += '<table id=\"footer_column-' + footer_column_row + '\" class=\"table table-clean table-footer-column\">';
\thtml += '<thead>';
\thtml += '<tr>';
\thtml += '<td width=\"96%\"><h4>Column Heading</h4></td>';
\thtml += '<td width=\"4%\">Sort Order</td>';
\thtml += '</tr>';
\thtml += '</thead>';
\thtml += '<tbody>';
   \thtml += '<tr id=\"link-row' + footer_column_row + '\">';
\thtml += '<td class=\"first\">';
\t";
        // line 203
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 204
            echo "\thtml += '<div class=\"input-group\">';
\thtml += '<span class=\"input-group-addon\"><img src=\"language/";
            // line 205
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_footer_columns][' + footer_column_row + '][title][";
            // line 206
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" />';
\thtml += '</div>';
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 209
        echo "\thtml += '</td>';
\thtml += '<td class=\"text-right\">';
\thtml += '<input type=\"text\" class=\"form-control\" style=\"width:60px\" value=\"' + footer_column_row + '\" name=\"settings[basel][basel_footer_columns][' + footer_column_row + '][sort]\" />';
\thtml += '</td>';
\thtml += '</tr>';
\thtml += '</tbody>';
\thtml += '</table>';
\thtml += '<table id=\"links-holder-' + footer_column_row  + '\" class=\"table table-clean\">';
    html += '<thead>';
\thtml += '<tr><td width=\"48%\"><h4>Column Links</h4></td>';
\thtml += '<td width=\"48%\"><span data-toggle=\"tooltip\" title=\"Include http:// when linking to external targets\">Link Target</span></td>';
\thtml += '<td width=\"4%\"><span style=\"white-space:nowrap\">Sort Order</span></td></tr>';
\thtml += '</thead>';
\thtml += '<tbody>';
\thtml += '<tr></tr>';
\thtml += '</tbody>';
\thtml += '<tfoot>';
\thtml += '<tr>';
\thtml += '<td colspan=\"3\"></td>';
\thtml += '<td class=\"text-right\"><button type=\"button\" onclick=\"addFooterLink(' + footer_column_row  + ');\" class=\"btn btn-primary\">Add Link</button></td>';
\thtml += '</tr>';
\thtml += '</tfoot>';
\thtml += '</table>';
\thtml += '</div>';
\thtml += '</div>';
\t\$('#footer-columns-holder').append(html);
\t\$('#column-add').before('<li><a href=\"#footer-column-' + footer_column_row + '\" data-toggle=\"tab\"><i class=\"fa fa-minus-circle\" onclick=\"\$(\\'a[href=\\\\\\'#footer-column-' + footer_column_row + '\\\\\\']\\').parent().remove(); \$(\\'#footer-column-' + footer_column_row + '\\').remove(); \$(\\'#footer-column-tabs a:first\\').tab(\\'show\\');\"></i> Column ' + footer_column_row + '</a></li>');
\t\$('#footer-column-tabs a[href=\\'#footer-column-' + footer_column_row + '\\']').tab('show');
\tfooter_column_row++;
\t\$('[data-toggle=\\'tooltip\\']').tooltip({container: 'body'});
}
//--></script>

<script type=\"text/javascript\"><!--
function addFooterLink(footer_column_row) {
\tlink_row = \$('#links-holder-' + footer_column_row + ' tbody tr').length;
\thtml = '<tr id=\"footer-link-row-' + footer_column_row + '-' + link_row + '\">';
\thtml += '<td class=\"first\">';
\t";
        // line 247
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 248
            echo "\thtml += '<div class=\"input-group\">';
\thtml += '<span class=\"input-group-addon\"><img src=\"language/";
            // line 249
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_footer_columns][' + footer_column_row + '][links][' + link_row + '][title][";
            // line 250
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" />';
\thtml += '</div>';
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 253
        echo "\thtml += '</td>';
\thtml += '<td class=\"first\">';
\thtml += '<input type=\"text\" class=\"form-control\" name=\"settings[basel][basel_footer_columns][' + footer_column_row + '][links][' + link_row + '][target]\" />';
\thtml += '</td>';
\thtml += '<td class=\"first\">';
\thtml += '<input type=\"text\" style=\"width:60px\" class=\"form-control\" name=\"settings[basel][basel_footer_columns][' + footer_column_row + '][links][' + link_row + '][sort]\" value=\"' + link_row + '\" />';
\thtml += '</td>';
\thtml += '<td class=\"text-right\">';
\thtml += '<button type=\"button\" onclick=\"\$(\\'#footer-link-row-' + footer_column_row  + '-' + link_row + '\\').remove();\" class=\"btn btn-danger\">Remove</button>';
\thtml += '</td>';
   \thtml += '</tr>';
\t\$('#links-holder-' + footer_column_row + ' tbody').append(html);
\tlink_row++;
}
//--></script>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  612 => 253,  603 => 250,  595 => 249,  592 => 248,  588 => 247,  548 => 209,  539 => 206,  531 => 205,  528 => 204,  524 => 203,  507 => 189,  500 => 184,  489 => 181,  481 => 180,  477 => 178,  473 => 177,  461 => 168,  455 => 167,  448 => 162,  434 => 156,  428 => 155,  422 => 154,  408 => 147,  401 => 145,  391 => 140,  382 => 135,  378 => 134,  374 => 132,  357 => 130,  353 => 129,  344 => 122,  337 => 119,  328 => 113,  322 => 109,  318 => 107,  312 => 106,  304 => 103,  294 => 100,  284 => 97,  280 => 95,  265 => 92,  257 => 91,  254 => 90,  250 => 89,  242 => 87,  237 => 86,  234 => 85,  232 => 84,  219 => 74,  209 => 69,  205 => 67,  192 => 64,  184 => 63,  181 => 62,  177 => 61,  172 => 59,  158 => 49,  154 => 48,  152 => 47,  150 => 46,  145 => 43,  139 => 42,  137 => 41,  126 => 40,  121 => 39,  119 => 38,  107 => 33,  97 => 28,  91 => 27,  82 => 20,  67 => 15,  60 => 13,  54 => 11,  50 => 10,  46 => 8,  29 => 6,  25 => 5,  19 => 1,);
    }
}
/* <legend>Footer</legend>*/
/* */
/* <legend class="sub">Footer Custom Block (top)</legend>*/
/* <ul class="nav nav-tabs language-tabs">*/
/* {% for language in languages %}*/
/* <li><a href="#footer-block1{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /> {{ language.name }}</a></li>*/
/* {% endfor %}*/
/* </ul>*/
/* <div class="tab-content">*/
/* {% for language in languages %}*/
/* <div class="tab-pane" id="footer-block1{{ language.language_id }}">*/
/*   <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Custom block which will take place above the footer links">Footer custom block (top)</span><br /><a class="editor-toggle" id="enable-editor-footer1-{{ language.language_id }}" onclick="enable_editor('footer1','{{ language.language_id }}')">Enable HTML editor</a></label>*/
/*     <div class="col-sm-10">*/
/*       <textarea id="editor-textarea-footer1-{{ language.language_id }}" name="settings[basel][footer_block_1][{{ language.language_id }}]" class="form-control code">{{ footer_block_1[language.language_id] ? footer_block_1[language.language_id] }}</textarea>*/
/*     </div>*/
/*   </div>*/
/*  </div>*/
/* {% endfor %}*/
/* </div>*/
/* */
/*           */
/* <legend class="sub">Footer Links</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable to use custom footer links instead of the default ones in Opencart">Overwrite default links</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][overwrite_footer_links]" class="footer-custom-links-select" value="0" {% if overwrite_footer_links == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][overwrite_footer_links]" class="footer-custom-links-select" value="1" {% if overwrite_footer_links == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <div id="custom_footer_links_holder"{% if overwrite_footer_links %} style="display:block"{% else %} style="display:none"{% endif %}>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"></label>*/
/* <div class="col-sm-10">*/
/* <ul class="nav content-tabs" id="footer-column-tabs">*/
/*     {% set footer_column_row = 1 %}*/
/*     {% for basel_footer_column in basel_footer_columns %}*/
/*     <li><a href="#footer-column-{{ footer_column_row }}" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#footer-column-{{ footer_column_row }}\']').parent().remove(); $('#footer-column-{{ footer_column_row }}').remove(); $('#footer-column-tabs a:first').tab('show');"></i> Column {{ footer_column_row }}</a></li>*/
/*     {% set footer_column_row = footer_column_row + 1 %}*/
/*     {% endfor %}*/
/*     <li id="column-add" style="cursor:pointer;"><a onclick="addFooterColumn();"><i class="fa fa-plus-circle"></i> Add Column</a></li> */
/* </ul>*/
/* <div class="tab-content" id="footer-columns-holder">*/
/* {% set footer_column_row = 1 %}*/
/* {% if basel_footer_columns is defined %}*/
/* {% for basel_footer_column in basel_footer_columns %}*/
/* <div class="tab-pane" id="footer-column-{{ footer_column_row }}">*/
/* <div class="tab-content">*/
/* <table class="table table-clean table-footer-column">*/
/* <thead>*/
/*   <tr>*/
/*     <td width="96%"><h4>Column Heading</h4></td>*/
/*     <td width="4%">Sort Order</td>*/
/*   </tr>*/
/* </thead>*/
/* <tbody>*/
/*   <tr id="column-row{{ footer_column_row }}">*/
/*     <td class="first">*/
/*     {% for language in languages %}*/
/*     <div class="input-group">*/
/*     <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*     <input type="text" class="form-control" name="settings[basel][basel_footer_columns][{{ footer_column_row }}][title][{{ language.language_id }}]" value="{{ basel_footer_column.title[language.language_id] ? basel_footer_column.title[language.language_id] }}" />*/
/*     </div>*/
/*     {% endfor %}*/
/*     </td>*/
/*     <td class="text-right">*/
/*     <input type="text" class="form-control" style="width:60px" name="settings[basel][basel_footer_columns][{{ footer_column_row }}][sort]" value="{{ basel_footer_column.sort ? basel_footer_column.sort : '99' }}" />*/
/*     </td>*/
/*   </tr>*/
/* </tbody>*/
/* </table>*/
/* <table id="links-holder-{{ footer_column_row }}" class="table table-clean">*/
/*     <thead>*/
/*       <tr>*/
/*         <td width="48%"><h4>Column Links</h4></td>*/
/*         <td width="48%"><span data-toggle="tooltip" title="Include http:// when linking to external targets">Link Target</span></td>*/
/*         <td width="4%"><span style="white-space:nowrap">Sort Order</span></td>*/
/*       </tr>*/
/*     </thead>*/
/*     <tbody>*/
/*     <tr></tr>*/
/*     {% set footer_link_row = 1 %}*/
/* 	{% if basel_footer_column.links is defined %}*/
/* 	{% for link in basel_footer_column.links %}*/
/*     <tr id="footer-link-row-{{ footer_column_row }}-{{ footer_link_row }}">*/
/*     <td class="first">*/
/*     {% for language in languages %}*/
/*     <div class="input-group">*/
/*     <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*     <input class="form-control" name="settings[basel][basel_footer_columns][{{ footer_column_row }}][links][{{ footer_link_row }}][title][{{ language.language_id }}]" value="{{ link.title[language.language_id] ? link.title[language.language_id] }}" />*/
/*     </div>*/
/*     {% endfor %}*/
/*     </td>*/
/*     <td class="first">*/
/*     <input class="form-control" name="settings[basel][basel_footer_columns][{{ footer_column_row }}][links][{{ footer_link_row }}][target]" value="{{ link.target ? link.target }}" />*/
/*     </td>*/
/*     <td class="first">*/
/*     <input type="text" class="form-control" style="width:60px" name="settings[basel][basel_footer_columns][{{ footer_column_row }}][links][{{ footer_link_row }}][sort]" value="{{ link.sort ? link.sort : '1' }}" />*/
/*     </td>*/
/*     <td class="text-right">*/
/*     <button type="button" onclick="$('#footer-link-row-{{ footer_column_row }}-{{ footer_link_row }}').remove();" class="btn btn-danger">Remove</button>*/
/*     </td>*/
/*     </tr>*/
/* {% set footer_link_row = footer_link_row + 1 %}*/
/* {% endfor %} <!-- foreach groups ends -->*/
/* {% endif %}*/
/* </tbody>*/
/*     <tfoot>*/
/*       <tr>*/
/*         <td colspan="3"></td>*/
/*         <td class="text-right"><button type="button" onclick="addFooterLink({{ footer_column_row }});" class="btn btn-primary">Add Link</button></td>*/
/*       </tr>*/
/*     </tfoot>*/
/*   </table>*/
/* </div>*/
/* </div> <!-- #footer-columns--->*/
/* {% set footer_column_row = footer_column_row + 1 %}*/
/* {% endfor %}*/
/* {% endif %}*/
/* </div>  <!-- #footer-columns-holder-->*/
/* </div>                */
/* </div>*/
/* </div>*/
/* */
/* <legend class="sub">Footer Custom Block (right)</legend>*/
/* <ul class="nav nav-tabs language-tabs">*/
/* {% for language in languages %}*/
/* <li><a href="#footer-block2-language{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /> {{ language.name }}</a></li>*/
/* {% endfor %}*/
/* </ul>*/
/* <div class="tab-content">*/
/* {% for language in languages %}*/
/* <div class="tab-pane" id="footer-block2-language{{ language.language_id }}">*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Heading</label>*/
/* <div class="col-sm-10">*/
/*   <input class="form-control" name="settings[basel][footer_block_title][{{ language.language_id }}]" value="{{ footer_block_title[language.language_id] ? footer_block_title[language.language_id] }}" />*/
/* </div>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Custom block which will take place as a column right by the footer links">Footer custom block (top)</span><br /><a class="editor-toggle" id="enable-editor-footer2-{{ language.language_id }}" onclick="enable_editor('footer2','{{ language.language_id }}')">Enable HTML editor</a></label>*/
/* <div class="col-sm-10">*/
/*   <textarea id="editor-textarea-footer2-{{ language.language_id }}" name="settings[basel][footer_block_2][{{ language.language_id }}]" class="form-control code">{{ footer_block_2[language.language_id] ? footer_block_2[language.language_id] }}</textarea>*/
/* </div>*/
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Contact detail lines will take place below the custom block">Contact details</span></label>*/
/* <div class="col-sm-10">*/
/*   <input class="form-control" name="settings[basel][footer_infoline_1][{{ language.language_id }}]" value="{{ footer_infoline_1[language.language_id] ? footer_infoline_1[language.language_id] }}" />*/
/*   <input class="form-control" name="settings[basel][footer_infoline_2][{{ language.language_id }}]" value="{{ footer_infoline_2[language.language_id] ? footer_infoline_2[language.language_id] }}" />*/
/*   <input class="form-control" name="settings[basel][footer_infoline_3][{{ language.language_id }}]" value="{{ footer_infoline_3[language.language_id] ? footer_infoline_3[language.language_id] }}" />*/
/* </div>*/
/* </div>*/
/*   */
/*  </div>*/
/* {% endfor %}*/
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="If an image is uploaded, it will take place below the contact details">Payment icons</span></label>*/
/*     <div class="col-sm-10">*/
/*     <a href="" id="thumb-payment-img" data-toggle="image" class="img-thumbnail"><img src="{{ payment_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*     <input type="hidden" name="settings[basel][basel_payment_img]" value="{{ basel_payment_img }}" id="input-payment-img" />*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <legend class="sub">Footer Copyright Text</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Use {year} to add the current year">Footer Copyright Text</span></label>*/
/*     <div class="col-sm-10">*/
/*     {% for language in languages %}*/
/*     <div class="input-group">*/
/*     <span class="input-group-addon">*/
/*     <img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.language_id }}" /></span>*/
/*     <input class="form-control" name="settings[basel][basel_copyright][{{ language.language_id }}]" value="{{ basel_copyright[language.language_id] ? basel_copyright[language.language_id] }}" />*/
/*     </div>*/
/*     {% endfor %}*/
/*     </div>*/
/* </div>*/
/* */
/* <script type="text/javascript"><!--*/
/* $('#footer-column-tabs a:first').tab('show');*/
/* var footer_column_row = {{ footer_column_row }};*/
/* function addFooterColumn() {*/
/* 	html = '<div class="tab-pane" id="footer-column-' + footer_column_row + '">';*/
/* 	html += '<div class="tab-content">';*/
/* 	html += '<table id="footer_column-' + footer_column_row + '" class="table table-clean table-footer-column">';*/
/* 	html += '<thead>';*/
/* 	html += '<tr>';*/
/* 	html += '<td width="96%"><h4>Column Heading</h4></td>';*/
/* 	html += '<td width="4%">Sort Order</td>';*/
/* 	html += '</tr>';*/
/* 	html += '</thead>';*/
/* 	html += '<tbody>';*/
/*    	html += '<tr id="link-row' + footer_column_row + '">';*/
/* 	html += '<td class="first">';*/
/* 	{% for language in languages %}*/
/* 	html += '<div class="input-group">';*/
/* 	html += '<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_footer_columns][' + footer_column_row + '][title][{{ language.language_id }}]" />';*/
/* 	html += '</div>';*/
/* 	{% endfor %}*/
/* 	html += '</td>';*/
/* 	html += '<td class="text-right">';*/
/* 	html += '<input type="text" class="form-control" style="width:60px" value="' + footer_column_row + '" name="settings[basel][basel_footer_columns][' + footer_column_row + '][sort]" />';*/
/* 	html += '</td>';*/
/* 	html += '</tr>';*/
/* 	html += '</tbody>';*/
/* 	html += '</table>';*/
/* 	html += '<table id="links-holder-' + footer_column_row  + '" class="table table-clean">';*/
/*     html += '<thead>';*/
/* 	html += '<tr><td width="48%"><h4>Column Links</h4></td>';*/
/* 	html += '<td width="48%"><span data-toggle="tooltip" title="Include http:// when linking to external targets">Link Target</span></td>';*/
/* 	html += '<td width="4%"><span style="white-space:nowrap">Sort Order</span></td></tr>';*/
/* 	html += '</thead>';*/
/* 	html += '<tbody>';*/
/* 	html += '<tr></tr>';*/
/* 	html += '</tbody>';*/
/* 	html += '<tfoot>';*/
/* 	html += '<tr>';*/
/* 	html += '<td colspan="3"></td>';*/
/* 	html += '<td class="text-right"><button type="button" onclick="addFooterLink(' + footer_column_row  + ');" class="btn btn-primary">Add Link</button></td>';*/
/* 	html += '</tr>';*/
/* 	html += '</tfoot>';*/
/* 	html += '</table>';*/
/* 	html += '</div>';*/
/* 	html += '</div>';*/
/* 	$('#footer-columns-holder').append(html);*/
/* 	$('#column-add').before('<li><a href="#footer-column-' + footer_column_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#footer-column-' + footer_column_row + '\\\']\').parent().remove(); $(\'#footer-column-' + footer_column_row + '\').remove(); $(\'#footer-column-tabs a:first\').tab(\'show\');"></i> Column ' + footer_column_row + '</a></li>');*/
/* 	$('#footer-column-tabs a[href=\'#footer-column-' + footer_column_row + '\']').tab('show');*/
/* 	footer_column_row++;*/
/* 	$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});*/
/* }*/
/* //--></script>*/
/* */
/* <script type="text/javascript"><!--*/
/* function addFooterLink(footer_column_row) {*/
/* 	link_row = $('#links-holder-' + footer_column_row + ' tbody tr').length;*/
/* 	html = '<tr id="footer-link-row-' + footer_column_row + '-' + link_row + '">';*/
/* 	html += '<td class="first">';*/
/* 	{% for language in languages %}*/
/* 	html += '<div class="input-group">';*/
/* 	html += '<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_footer_columns][' + footer_column_row + '][links][' + link_row + '][title][{{ language.language_id }}]" />';*/
/* 	html += '</div>';*/
/* 	{% endfor %}*/
/* 	html += '</td>';*/
/* 	html += '<td class="first">';*/
/* 	html += '<input type="text" class="form-control" name="settings[basel][basel_footer_columns][' + footer_column_row + '][links][' + link_row + '][target]" />';*/
/* 	html += '</td>';*/
/* 	html += '<td class="first">';*/
/* 	html += '<input type="text" style="width:60px" class="form-control" name="settings[basel][basel_footer_columns][' + footer_column_row + '][links][' + link_row + '][sort]" value="' + link_row + '" />';*/
/* 	html += '</td>';*/
/* 	html += '<td class="text-right">';*/
/* 	html += '<button type="button" onclick="$(\'#footer-link-row-' + footer_column_row  + '-' + link_row + '\').remove();" class="btn btn-danger">Remove</button>';*/
/* 	html += '</td>';*/
/*    	html += '</tr>';*/
/* 	$('#links-holder-' + footer_column_row + ' tbody').append(html);*/
/* 	link_row++;*/
/* }*/
/* //--></script>*/
