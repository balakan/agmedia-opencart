<?php

/* extension/basel/panel_tabs/shop.twig */
class __TwigTemplate_33dc1b032753a3c2124e98664af720688d8ec0991f8105e81fd96690294e49e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Shop</legend>

<legend class=\"sub\">Catalog Mode</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Disable the product purchase options globally\">Catlog Mode</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][catalog_mode]\" value=\"0\" ";
        // line 8
        if (((isset($context["catalog_mode"]) ? $context["catalog_mode"] : null) == "0")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Off</span></label>
    <label><input type=\"radio\" name=\"settings[basel][catalog_mode]\" value=\"1\" ";
        // line 9
        if (((isset($context["catalog_mode"]) ? $context["catalog_mode"] : null) == "1")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>On</span></label>
    </div>                   
</div>

<legend class=\"sub\">Product Quickview</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable/Disable the product quickview feature globally\">Product quickview Status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][quickview_status]\" value=\"0\" ";
        // line 17
        if (((isset($context["quickview_status"]) ? $context["quickview_status"] : null) == "0")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][quickview_status]\" value=\"1\" ";
        // line 18
        if (((isset($context["quickview_status"]) ? $context["quickview_status"] : null) == "1")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<legend class=\"sub\">Product Labels</legend>

<legend class=\"sub\">Add To Cart</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Sale label</label>
    <div class=\"col-sm-10\">
    <select name=\"settings[basel][salebadge_status]\" class=\"form-control\">
        <option value=\"0\"";
        // line 29
        if (((isset($context["salebadge_status"]) ? $context["salebadge_status"] : null) == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Disabled</option>
        <option value=\"1\"";
        // line 30
        if (((isset($context["salebadge_status"]) ? $context["salebadge_status"] : null) == "1")) {
            echo " selected=\"selected\"";
        }
        echo ">Enabled - Sale Text</option>
\t\t<option value=\"2\"";
        // line 31
        if (((isset($context["salebadge_status"]) ? $context["salebadge_status"] : null) == "2")) {
            echo " selected=\"selected\"";
        }
        echo ">Enabled - Discount Percentage</option>
    </select>
    </div>                   
</div>


<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Set how many days to show the new-label on new products. Set it as 0 to disable\">New label period</span></label>
    <div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[basel][newlabel_status]\" value=\"";
        // line 40
        echo (((isset($context["newlabel_status"]) ? $context["newlabel_status"] : null)) ? ((isset($context["newlabel_status"]) ? $context["newlabel_status"] : null)) : ("0"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Out of stock label</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][stock_badge_status]\" value=\"0\" ";
        // line 47
        if (((isset($context["stock_badge_status"]) ? $context["stock_badge_status"] : null) == "0")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][stock_badge_status]\" value=\"1\" ";
        // line 48
        if (((isset($context["stock_badge_status"]) ? $context["stock_badge_status"] : null) == "1")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                     
</div>

<legend class=\"sub\">Specials Countdown</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"View time left on (time limited) special prices. Please note tat the Product groups module has its own setting for this.\">Specials countdown status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][countdown_status]\" value=\"0\" ";
        // line 56
        if (((isset($context["countdown_status"]) ? $context["countdown_status"] : null) == "0")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][countdown_status]\" value=\"1\" ";
        // line 57
        if (((isset($context["countdown_status"]) ? $context["countdown_status"] : null) == "1")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<legend class=\"sub\">Add To Cart</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Action when an item is successfully added to cart\">Add to cart action</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <select name=\"settings[basel][basel_cart_action]\" class=\"form-control\">
        <option value=\"0\"";
        // line 66
        if (((isset($context["basel_cart_action"]) ? $context["basel_cart_action"] : null) == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Stay - Notification message</option>
        <option value=\"redirect_cart\"";
        // line 67
        if (((isset($context["basel_cart_action"]) ? $context["basel_cart_action"] : null) == "redirect_cart")) {
            echo " selected=\"selected\"";
        }
        echo ">Redirect - Shopping cart</option>
        <option value=\"redirect_checkout\"";
        // line 68
        if (((isset($context["basel_cart_action"]) ? $context["basel_cart_action"] : null) == "redirect_checkout")) {
            echo " selected=\"selected\"";
        }
        echo ">Redirect - Checkout</option>
    </select>
    </div>                   
</div>

<legend class=\"sub\">Wish List</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable/Disable the wishlist feature globally\">Wish List Status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][wishlist_status]\" value=\"0\" ";
        // line 77
        if (((isset($context["wishlist_status"]) ? $context["wishlist_status"] : null) == "0")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][wishlist_status]\" value=\"1\" ";
        // line 78
        if (((isset($context["wishlist_status"]) ? $context["wishlist_status"] : null) == "1")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Action when an item is successfully added to wish list\">Add to wish list action</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <select name=\"settings[basel][basel_wishlist_action]\" class=\"form-control\">
        <option value=\"0\"";
        // line 86
        if (((isset($context["basel_wishlist_action"]) ? $context["basel_wishlist_action"] : null) == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Stay - Notification message</option>
        <option value=\"redirect\"";
        // line 87
        if (((isset($context["basel_wishlist_action"]) ? $context["basel_wishlist_action"] : null) == "redirect")) {
            echo " selected=\"selected\"";
        }
        echo ">Redirect - Wish List Page</option>
    </select>
    </div>                   
</div>

<legend class=\"sub\">Product Comparison</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable/Disable the product comparison feature globally\">Comparison Status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][compare_status]\" value=\"0\" ";
        // line 96
        if (((isset($context["compare_status"]) ? $context["compare_status"] : null) == "0")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][compare_status]\" value=\"1\" ";
        // line 97
        if (((isset($context["compare_status"]) ? $context["compare_status"] : null) == "1")) {
            echo "checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Action when an item is successfully added to wish list\">Add to compare action</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <select name=\"settings[basel][basel_compare_action]\" class=\"form-control\">
        <option value=\"0\"";
        // line 105
        if (((isset($context["basel_compare_action"]) ? $context["basel_compare_action"] : null) == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Stay - Notification message</option>
        <option value=\"redirect\"";
        // line 106
        if (((isset($context["basel_compare_action"]) ? $context["basel_compare_action"] : null) == "redirect")) {
            echo " selected=\"selected\"";
        }
        echo ">Redirect - Product Comparison Page</option>
    </select>
    </div>                   
</div>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/shop.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 106,  228 => 105,  215 => 97,  209 => 96,  195 => 87,  189 => 86,  176 => 78,  170 => 77,  156 => 68,  150 => 67,  144 => 66,  130 => 57,  124 => 56,  111 => 48,  105 => 47,  95 => 40,  81 => 31,  75 => 30,  69 => 29,  53 => 18,  47 => 17,  34 => 9,  28 => 8,  19 => 1,);
    }
}
/* <legend>Shop</legend>*/
/* */
/* <legend class="sub">Catalog Mode</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Disable the product purchase options globally">Catlog Mode</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][catalog_mode]" value="0" {% if catalog_mode == '0' %}checked="checked"{% endif %} /><span>Off</span></label>*/
/*     <label><input type="radio" name="settings[basel][catalog_mode]" value="1" {% if catalog_mode == '1' %}checked="checked"{% endif %} /><span>On</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Product Quickview</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable/Disable the product quickview feature globally">Product quickview Status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][quickview_status]" value="0" {% if quickview_status == '0' %}checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][quickview_status]" value="1" {% if quickview_status == '1' %}checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Product Labels</legend>*/
/* */
/* <legend class="sub">Add To Cart</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Sale label</label>*/
/*     <div class="col-sm-10">*/
/*     <select name="settings[basel][salebadge_status]" class="form-control">*/
/*         <option value="0"{% if salebadge_status == '0' %} selected="selected"{% endif %}>Disabled</option>*/
/*         <option value="1"{% if salebadge_status == '1' %} selected="selected"{% endif %}>Enabled - Sale Text</option>*/
/* 		<option value="2"{% if salebadge_status == '2' %} selected="selected"{% endif %}>Enabled - Discount Percentage</option>*/
/*     </select>*/
/*     </div>                   */
/* </div>*/
/* */
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Set how many days to show the new-label on new products. Set it as 0 to disable">New label period</span></label>*/
/*     <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[basel][newlabel_status]" value="{{ newlabel_status ? newlabel_status : '0' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Out of stock label</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][stock_badge_status]" value="0" {% if stock_badge_status == '0' %}checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][stock_badge_status]" value="1" {% if stock_badge_status == '1' %}checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                     */
/* </div>*/
/* */
/* <legend class="sub">Specials Countdown</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="View time left on (time limited) special prices. Please note tat the Product groups module has its own setting for this.">Specials countdown status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][countdown_status]" value="0" {% if countdown_status == '0' %}checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][countdown_status]" value="1" {% if countdown_status == '1' %}checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Add To Cart</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Action when an item is successfully added to cart">Add to cart action</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <select name="settings[basel][basel_cart_action]" class="form-control">*/
/*         <option value="0"{% if basel_cart_action == '0' %} selected="selected"{% endif %}>Stay - Notification message</option>*/
/*         <option value="redirect_cart"{% if basel_cart_action == 'redirect_cart' %} selected="selected"{% endif %}>Redirect - Shopping cart</option>*/
/*         <option value="redirect_checkout"{% if basel_cart_action == 'redirect_checkout' %} selected="selected"{% endif %}>Redirect - Checkout</option>*/
/*     </select>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Wish List</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable/Disable the wishlist feature globally">Wish List Status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][wishlist_status]" value="0" {% if wishlist_status == '0' %}checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][wishlist_status]" value="1" {% if wishlist_status == '1' %}checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Action when an item is successfully added to wish list">Add to wish list action</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <select name="settings[basel][basel_wishlist_action]" class="form-control">*/
/*         <option value="0"{% if basel_wishlist_action == '0' %} selected="selected"{% endif %}>Stay - Notification message</option>*/
/*         <option value="redirect"{% if basel_wishlist_action == 'redirect' %} selected="selected"{% endif %}>Redirect - Wish List Page</option>*/
/*     </select>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Product Comparison</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable/Disable the product comparison feature globally">Comparison Status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][compare_status]" value="0" {% if compare_status == '0' %}checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][compare_status]" value="1" {% if compare_status == '1' %}checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Action when an item is successfully added to wish list">Add to compare action</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <select name="settings[basel][basel_compare_action]" class="form-control">*/
/*         <option value="0"{% if basel_compare_action == '0' %} selected="selected"{% endif %}>Stay - Notification message</option>*/
/*         <option value="redirect"{% if basel_compare_action == 'redirect' %} selected="selected"{% endif %}>Redirect - Product Comparison Page</option>*/
/*     </select>*/
/*     </div>                   */
/* </div>*/
