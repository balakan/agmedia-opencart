<?php

/* basel/template/common/content_top.twig */
class __TwigTemplate_91b7da92a809881afd00f6f3bcfa5056589ffeaa557547605853c971d8275fdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["modules"]) ? $context["modules"] : null)) {
            // line 2
            echo "<div class=\"pos_content_top\">
<div class=\"inner\">
";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 5
                echo $context["module"];
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "</div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "basel/template/common/content_top.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 7,  29 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if modules %}*/
/* <div class="pos_content_top">*/
/* <div class="inner">*/
/* {% for module in modules %}*/
/* {{ module }}*/
/* {% endfor %}*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
