<?php

/* extension/basel/panel_tabs/product-pages.twig */
class __TwigTemplate_ea75271d7bfc31fa2bd373a07873c543eb8b763f36f4acce6522724d81c68911 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<legend>Product Pages</legend>

<legend class=\"sub\">Product Images</legend>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Images layout</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][product_layout]\" class=\"form-control\">
        <option value=\"images-left\"";
        // line 9
        if (((isset($context["product_layout"]) ? $context["product_layout"] : null) == "images-left")) {
            echo " selected=\"selected\"";
        }
        echo ">Additional images left of main image</option>
        <option value=\"images-bottom\"";
        // line 10
        if (((isset($context["product_layout"]) ? $context["product_layout"] : null) == "images-bottom")) {
            echo " selected=\"selected\"";
        }
        echo ">Additional images below main image</option>
        <option value=\"full-width\"";
        // line 11
        if (((isset($context["product_layout"]) ? $context["product_layout"] : null) == "full-width")) {
            echo " selected=\"selected\"";
        }
        echo ">Stacked images - (Full width layout)</option>
    </select>
</div>                   
</div>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Zoom main image on hover</label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_hover_zoom]\" value=\"0\" ";
        // line 18
        if (((isset($context["basel_hover_zoom"]) ? $context["basel_hover_zoom"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_hover_zoom]\" value=\"1\" ";
        // line 19
        if (((isset($context["basel_hover_zoom"]) ? $context["basel_hover_zoom"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<legend class=\"sub\">Product Details</legend>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Show the product meta description among the product details\">View meta description</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][meta_description_status]\" value=\"0\" ";
        // line 28
        if (((isset($context["meta_description_status"]) ? $context["meta_description_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][meta_description_status]\" value=\"1\" ";
        // line 29
        if (((isset($context["meta_description_status"]) ? $context["meta_description_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Show a countdown until special prices expires. Please note that there is a separate setting to show countdown on product listings in the Shop tab\">Specials countdown</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][product_page_countdown]\" value=\"0\" ";
        // line 36
        if (((isset($context["product_page_countdown"]) ? $context["product_page_countdown"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][product_page_countdown]\" value=\"1\" ";
        // line 37
        if (((isset($context["product_page_countdown"]) ? $context["product_page_countdown"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Share buttons</label>
<div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_share_btn]\" value=\"0\" ";
        // line 44
        if (((isset($context["basel_share_btn"]) ? $context["basel_share_btn"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_share_btn]\" value=\"1\" ";
        // line 45
        if (((isset($context["basel_share_btn"]) ? $context["basel_share_btn"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                    
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Sharing buttons style</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_sharing_style]\" class=\"form-control\">
        <option value=\"small\"";
        // line 53
        if (((isset($context["basel_sharing_style"]) ? $context["basel_sharing_style"] : null) == "small")) {
            echo " selected=\"selected\"";
        }
        echo ">Default (small)</option>
        <option value=\"large\"";
        // line 54
        if (((isset($context["basel_sharing_style"]) ? $context["basel_sharing_style"] : null) == "large")) {
            echo " selected=\"selected\"";
        }
        echo ">Large</option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable/Disable price excluding tax on product pages and in list view\">View ex tax price</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][ex_tax_status]\" value=\"0\" ";
        // line 62
        if (((isset($context["ex_tax_status"]) ? $context["ex_tax_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][ex_tax_status]\" value=\"1\" ";
        // line 63
        if (((isset($context["ex_tax_status"]) ? $context["ex_tax_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Enable/Disable the price to be updated after selecting options and quantities\">Live price update</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][basel_price_update]\" value=\"0\" ";
        // line 70
        if (((isset($context["basel_price_update"]) ? $context["basel_price_update"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][basel_price_update]\" value=\"1\" ";
        // line 71
        if (((isset($context["basel_price_update"]) ? $context["basel_price_update"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<legend class=\"sub\">Product Information Tabs</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Tabs layout</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][full_width_tabs]\" class=\"form-control\">
        <option value=\"1\"";
        // line 80
        if (((isset($context["full_width_tabs"]) ? $context["full_width_tabs"] : null) == "1")) {
            echo " selected=\"selected\"";
        }
        echo ">Full width with contrast background</option>
        <option value=\"0\"";
        // line 81
        if (((isset($context["full_width_tabs"]) ? $context["full_width_tabs"] : null) == "0")) {
            echo " selected=\"selected\"";
        }
        echo ">Inline</option>
    </select>
</div>                   
</div>

<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Tabs Style</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][product_tabs_style]\" class=\"form-control\">
    \t<option value=\"\"";
        // line 90
        if (((isset($context["product_tabs_style"]) ? $context["product_tabs_style"] : null) == "")) {
            echo " selected=\"selected\"";
        }
        echo ">Default Style</option>
        <option value=\"nav-tabs-lg text-center\"";
        // line 91
        if (((isset($context["product_tabs_style"]) ? $context["product_tabs_style"] : null) == "nav-tabs-lg text-center")) {
            echo " selected=\"selected\"";
        }
        echo ">Centered (Large)</option>
        <option value=\"nav-tabs-sm text-center\"";
        // line 92
        if (((isset($context["product_tabs_style"]) ? $context["product_tabs_style"] : null) == "nav-tabs-sm text-center")) {
            echo " selected=\"selected\"";
        }
        echo ">Centered (Small)</option>
    </select>
</div>                   
</div>




<legend class=\"sub\">Product Questions &amp; Answers</legend>
<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Allow customers to ask questions/see previously asked questions about products directly on product pages\">Product Questions Status</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][product_question_status]\" value=\"0\" ";
        // line 104
        if (((isset($context["product_question_status"]) ? $context["product_question_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Disabled</span></label>
    <label><input type=\"radio\" name=\"settings[basel][product_question_status]\" value=\"1\" ";
        // line 105
        if (((isset($context["product_question_status"]) ? $context["product_question_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Enabled</span></label>
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\">Questions Per Page</label>
    <div class=\"col-sm-10\">
    <input class=\"form-control\" name=\"settings[basel][questions_per_page]\" value=\"";
        // line 112
        echo (((isset($context["questions_per_page"]) ? $context["questions_per_page"] : null)) ? ((isset($context["questions_per_page"]) ? $context["questions_per_page"] : null)) : ("5"));
        echo "\" />
    </div>                   
</div>

<div class=\"form-group\">
    <label class=\"col-sm-2 control-label\"><span data-toggle=\"tooltip\" title=\"Set the status on new questions asked. If set to published, not yet answered questions will be visible\">Status On New Questions</span></label>
    <div class=\"col-sm-10 toggle-btn\">
    <label><input type=\"radio\" name=\"settings[basel][questions_new_status]\" value=\"0\" ";
        // line 119
        if (((isset($context["questions_new_status"]) ? $context["questions_new_status"] : null) == "0")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Not Published</span></label>
    <label><input type=\"radio\" name=\"settings[basel][questions_new_status]\" value=\"1\" ";
        // line 120
        if (((isset($context["questions_new_status"]) ? $context["questions_new_status"] : null) == "1")) {
            echo " checked=\"checked\"";
        }
        echo " /><span>Published</span></label>
    </div>                  
</div>

<legend class=\"sub\">Related Products</legend>
<div class=\"form-group\">
<label class=\"col-sm-2 control-label\">Products per row</label>
<div class=\"col-sm-10\">
    <select name=\"settings[basel][basel_rel_prod_grid]\" class=\"form-control\">
        <option value=\"2\"";
        // line 129
        if (((isset($context["basel_rel_prod_grid"]) ? $context["basel_rel_prod_grid"] : null) == "2")) {
            echo " selected=\"selected\"";
        }
        echo ">2</option>
        <option value=\"3\"";
        // line 130
        if (((isset($context["basel_rel_prod_grid"]) ? $context["basel_rel_prod_grid"] : null) == "3")) {
            echo " selected=\"selected\"";
        }
        echo ">3</option>
        <option value=\"4\"";
        // line 131
        if (((isset($context["basel_rel_prod_grid"]) ? $context["basel_rel_prod_grid"] : null) == "4")) {
            echo " selected=\"selected\"";
        }
        echo ">4</option>
        <option value=\"5\"";
        // line 132
        if (((isset($context["basel_rel_prod_grid"]) ? $context["basel_rel_prod_grid"] : null) == "5")) {
            echo " selected=\"selected\"";
        }
        echo ">5</option>
    </select>
</div>                   
</div>";
    }

    public function getTemplateName()
    {
        return "extension/basel/panel_tabs/product-pages.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 132,  294 => 131,  288 => 130,  282 => 129,  268 => 120,  262 => 119,  252 => 112,  240 => 105,  234 => 104,  217 => 92,  211 => 91,  205 => 90,  191 => 81,  185 => 80,  171 => 71,  165 => 70,  153 => 63,  147 => 62,  134 => 54,  128 => 53,  115 => 45,  109 => 44,  97 => 37,  91 => 36,  79 => 29,  73 => 28,  59 => 19,  53 => 18,  41 => 11,  35 => 10,  29 => 9,  19 => 1,);
    }
}
/* <legend>Product Pages</legend>*/
/* */
/* <legend class="sub">Product Images</legend>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Images layout</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][product_layout]" class="form-control">*/
/*         <option value="images-left"{% if product_layout == 'images-left' %} selected="selected"{% endif %}>Additional images left of main image</option>*/
/*         <option value="images-bottom"{% if product_layout == 'images-bottom' %} selected="selected"{% endif %}>Additional images below main image</option>*/
/*         <option value="full-width"{% if product_layout == 'full-width' %} selected="selected"{% endif %}>Stacked images - (Full width layout)</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Zoom main image on hover</label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_hover_zoom]" value="0" {% if basel_hover_zoom == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_hover_zoom]" value="1" {% if basel_hover_zoom == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Product Details</legend>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Show the product meta description among the product details">View meta description</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][meta_description_status]" value="0" {% if meta_description_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][meta_description_status]" value="1" {% if meta_description_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Show a countdown until special prices expires. Please note that there is a separate setting to show countdown on product listings in the Shop tab">Specials countdown</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][product_page_countdown]" value="0" {% if product_page_countdown == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][product_page_countdown]" value="1" {% if product_page_countdown == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Share buttons</label>*/
/* <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_share_btn]" value="0" {% if basel_share_btn == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_share_btn]" value="1" {% if basel_share_btn == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                    */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Sharing buttons style</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_sharing_style]" class="form-control">*/
/*         <option value="small"{% if basel_sharing_style == 'small' %} selected="selected"{% endif %}>Default (small)</option>*/
/*         <option value="large"{% if basel_sharing_style == 'large' %} selected="selected"{% endif %}>Large</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable/Disable price excluding tax on product pages and in list view">View ex tax price</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][ex_tax_status]" value="0" {% if ex_tax_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][ex_tax_status]" value="1" {% if ex_tax_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Enable/Disable the price to be updated after selecting options and quantities">Live price update</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][basel_price_update]" value="0" {% if basel_price_update == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][basel_price_update]" value="1" {% if basel_price_update == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <legend class="sub">Product Information Tabs</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Tabs layout</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][full_width_tabs]" class="form-control">*/
/*         <option value="1"{% if full_width_tabs == '1' %} selected="selected"{% endif %}>Full width with contrast background</option>*/
/*         <option value="0"{% if full_width_tabs == '0' %} selected="selected"{% endif %}>Inline</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Tabs Style</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][product_tabs_style]" class="form-control">*/
/*     	<option value=""{% if product_tabs_style == '' %} selected="selected"{% endif %}>Default Style</option>*/
/*         <option value="nav-tabs-lg text-center"{% if product_tabs_style == 'nav-tabs-lg text-center' %} selected="selected"{% endif %}>Centered (Large)</option>*/
/*         <option value="nav-tabs-sm text-center"{% if product_tabs_style == 'nav-tabs-sm text-center' %} selected="selected"{% endif %}>Centered (Small)</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
/* */
/* */
/* */
/* */
/* <legend class="sub">Product Questions &amp; Answers</legend>*/
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Allow customers to ask questions/see previously asked questions about products directly on product pages">Product Questions Status</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][product_question_status]" value="0" {% if product_question_status == '0' %} checked="checked"{% endif %} /><span>Disabled</span></label>*/
/*     <label><input type="radio" name="settings[basel][product_question_status]" value="1" {% if product_question_status == '1' %} checked="checked"{% endif %} /><span>Enabled</span></label>*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label">Questions Per Page</label>*/
/*     <div class="col-sm-10">*/
/*     <input class="form-control" name="settings[basel][questions_per_page]" value="{{ questions_per_page ? questions_per_page : '5' }}" />*/
/*     </div>                   */
/* </div>*/
/* */
/* <div class="form-group">*/
/*     <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="Set the status on new questions asked. If set to published, not yet answered questions will be visible">Status On New Questions</span></label>*/
/*     <div class="col-sm-10 toggle-btn">*/
/*     <label><input type="radio" name="settings[basel][questions_new_status]" value="0" {% if questions_new_status == '0' %} checked="checked"{% endif %} /><span>Not Published</span></label>*/
/*     <label><input type="radio" name="settings[basel][questions_new_status]" value="1" {% if questions_new_status == '1' %} checked="checked"{% endif %} /><span>Published</span></label>*/
/*     </div>                  */
/* </div>*/
/* */
/* <legend class="sub">Related Products</legend>*/
/* <div class="form-group">*/
/* <label class="col-sm-2 control-label">Products per row</label>*/
/* <div class="col-sm-10">*/
/*     <select name="settings[basel][basel_rel_prod_grid]" class="form-control">*/
/*         <option value="2"{% if basel_rel_prod_grid == '2' %} selected="selected"{% endif %}>2</option>*/
/*         <option value="3"{% if basel_rel_prod_grid == '3' %} selected="selected"{% endif %}>3</option>*/
/*         <option value="4"{% if basel_rel_prod_grid == '4' %} selected="selected"{% endif %}>4</option>*/
/*         <option value="5"{% if basel_rel_prod_grid == '5' %} selected="selected"{% endif %}>5</option>*/
/*     </select>*/
/* </div>                   */
/* </div>*/
