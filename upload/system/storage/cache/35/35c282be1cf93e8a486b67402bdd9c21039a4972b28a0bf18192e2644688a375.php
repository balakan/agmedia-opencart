<?php

/* basel/template/common/menu.twig */
class __TwigTemplate_64748488dc6b17bbfafdfd6f3717a96e2a6d738f15074d0b8c99a829750a7484 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 3
                echo "    ";
                if ($this->getAttribute($context["category"], "children", array())) {
                    // line 4
                    echo "        <li class=\"has-sub dropdown-wrapper from-bottom\">
        <a href=\"";
                    // line 5
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\"><span class=\"top\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</span><i class=\"fa fa-angle-down\"></i></a>
        <div class=\"sub-holder dropdown-content dropdown-left\">
            <div class=\"dropdown-inner\"><div class=\"menu-item\">
\t\t\t\t";
                    // line 8
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), (twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array())) / twig_round($this->getAttribute($context["category"], "column", array()), 1, "ceil"))));
                    foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                        // line 9
                        echo "                  <ul class=\"default-menu-ul hover-menu\">
                    ";
                        // line 10
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($context["children"]);
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 11
                            echo "                    
\t\t\t\t\t\t";
                            // line 12
                            if ($this->getAttribute($context["child"], "grandchildren", array())) {
                                // line 13
                                echo "\t\t\t\t\t<li class=\"default-menu-li has-sub dropdown-wrapper from-bottom\"><a href=\"";
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "<i class=\"fa fa-angle-right\"></i></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-content sub-holder dropdown-left\">
\t\t\t\t\t\t\t\t";
                                // line 15
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "grandchildren", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["grandchild"]) {
                                    // line 16
                                    echo "\t\t\t\t\t\t\t\t\t<li class=\"default-menu-li\"><a href=\"";
                                    echo $this->getAttribute($context["grandchild"], "href", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["grandchild"], "name", array());
                                    echo "</a></li>
\t\t\t\t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grandchild'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 18
                                echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t</li>
\t\t\t\t\t\t";
                            } else {
                                // line 21
                                echo "\t\t\t\t\t<li class=\"default-menu-li\"><a href=\"";
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "</a></li>
\t\t\t\t\t\t";
                            }
                            // line 23
                            echo "\t\t\t\t\t
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 25
                        echo "                  </ul>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 27
                    echo "            </div>
        </div>
        </div>
        </li>
    ";
                } else {
                    // line 32
                    echo "        <li><a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></li>
    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "basel/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 32,  105 => 27,  98 => 25,  91 => 23,  83 => 21,  78 => 18,  67 => 16,  63 => 15,  55 => 13,  53 => 12,  50 => 11,  46 => 10,  43 => 9,  39 => 8,  31 => 5,  28 => 4,  25 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if categories %}*/
/* {% for category in categories %}*/
/*     {% if category.children %}*/
/*         <li class="has-sub dropdown-wrapper from-bottom">*/
/*         <a href="{{ category.href }}"><span class="top">{{ category.name }}</span><i class="fa fa-angle-down"></i></a>*/
/*         <div class="sub-holder dropdown-content dropdown-left">*/
/*             <div class="dropdown-inner"><div class="menu-item">*/
/* 				{% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}*/
/*                   <ul class="default-menu-ul hover-menu">*/
/*                     {% for child in children %}*/
/*                     */
/* 						{% if child.grandchildren %}*/
/* 					<li class="default-menu-li has-sub dropdown-wrapper from-bottom"><a href="{{ child.href }}">{{ child.name }}<i class="fa fa-angle-right"></i></a>*/
/* 							<ul class="dropdown-content sub-holder dropdown-left">*/
/* 								{% for grandchild in child.grandchildren %}*/
/* 									<li class="default-menu-li"><a href="{{ grandchild.href }}">{{ grandchild.name }}</a></li>*/
/* 								{% endfor %}*/
/* 							</ul>*/
/* 					</li>*/
/* 						{% else %}*/
/* 					<li class="default-menu-li"><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/* 						{% endif %}*/
/* 					*/
/*                     {% endfor %}*/
/*                   </ul>*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/*         </div>*/
/*         </li>*/
/*     {% else %}*/
/*         <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*     {% endif %}*/
/* {% endfor %}*/
/* {% endif %}*/
