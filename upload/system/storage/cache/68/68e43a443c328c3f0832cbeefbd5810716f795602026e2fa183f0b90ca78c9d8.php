<?php

/* basel/template/common/header.twig */
class __TwigTemplate_49eaa954c1e0609e656ab1dda16ddc5ce07267fd6a7062fdba6441837a2bc5a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 12
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 13
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
";
        // line 14
        if ((isset($context["alternate"]) ? $context["alternate"] : null)) {
            echo (isset($context["alternate"]) ? $context["alternate"] : null);
        }
        echo " \t
\t\t\t

";
        // line 17
        if ((isset($context["canonical_link"]) ? $context["canonical_link"] : null)) {
            echo (("<link href=\"" . (isset($context["canonical_link"]) ? $context["canonical_link"] : null)) . "\" rel=\"canonical\" />");
        }
        echo " 
";
        // line 18
        if ((isset($context["robots"]) ? $context["robots"] : null)) {
            echo (isset($context["robots"]) ? $context["robots"] : null);
        }
        echo " 
\t\t\t
";
        // line 20
        if ((isset($context["description"]) ? $context["description"] : null)) {
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />";
        }
        // line 21
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            echo "<meta name=\"keywords\" content= \"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />";
        }
        // line 22
        echo "<!-- Load essential resources -->
<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\"></script>
<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/slick.min.js\"></script>
<script src=\"catalog/view/theme/basel/js/basel_common.js\"></script>
<!-- Main stylesheet -->
<link href=\"catalog/view/theme/basel/stylesheet/stylesheet.css\" rel=\"stylesheet\">
<!-- Mandatory Theme Settings CSS -->
<style id=\"basel-mandatory-css\">";
        // line 31
        echo (isset($context["basel_mandatory_css"]) ? $context["basel_mandatory_css"] : null);
        echo "</style>
<!-- Plugin Stylesheet(s) -->
";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 34
            echo "<link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        if (((isset($context["direction"]) ? $context["direction"] : null) == "rtl")) {
            // line 37
            echo "<link href=\"catalog/view/theme/basel/stylesheet/rtl.css\" rel=\"stylesheet\">
";
        }
        // line 39
        echo "<!-- Pluing scripts(s) -->
";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 41
            echo "<script src=\"";
            echo $context["script"];
            echo "\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "<!-- Page specific meta information -->
";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 45
            if (($this->getAttribute($context["link"], "rel", array()) == "image")) {
                // line 46
                echo "<meta property=\"og:image\" content=\"";
                echo $this->getAttribute($context["link"], "href", array());
                echo "\" />
";
            } else {
                // line 48
                echo "<link href=\"";
                echo $this->getAttribute($context["link"], "href", array());
                echo "\" rel=\"";
                echo $this->getAttribute($context["link"], "rel", array());
                echo "\" />
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "<!-- Analytic tools -->
";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 53
            echo $context["analytic"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        if ((isset($context["basel_styles_status"]) ? $context["basel_styles_status"] : null)) {
            // line 56
            echo "<!-- Custom Color Scheme -->
<style id=\"basel-color-scheme\">";
            // line 57
            echo (isset($context["basel_styles_cache"]) ? $context["basel_styles_cache"] : null);
            echo ";</style>
";
        }
        // line 59
        if ((isset($context["basel_typo_status"]) ? $context["basel_typo_status"] : null)) {
            // line 60
            echo "<!-- Custom Fonts -->
<style id=\"basel-fonts\">";
            // line 61
            echo (isset($context["basel_fonts_cache"]) ? $context["basel_fonts_cache"] : null);
            echo "</style>
";
        }
        // line 63
        if ((isset($context["basel_custom_css_status"]) ? $context["basel_custom_css_status"] : null)) {
            // line 64
            echo "<!-- Custom CSS -->
<style id=\"basel-custom-css\">
";
            // line 66
            echo (isset($context["basel_custom_css"]) ? $context["basel_custom_css"] : null);
            echo "
</style>
";
        }
        // line 69
        if ((isset($context["basel_custom_js_status"]) ? $context["basel_custom_js_status"] : null)) {
            // line 70
            echo "<!-- Custom Javascript -->
<script>
";
            // line 72
            echo (isset($context["basel_custom_js"]) ? $context["basel_custom_js"] : null);
            echo "
</script>
";
        }
        // line 75
        echo "
\t\t\t<link rel=\"stylesheet\" href=\"catalog/view/javascript/jquery.cluetip.css\" type=\"text/css\" />
\t\t\t<script src=\"catalog/view/javascript/jquery.cluetip.js\" type=\"text/javascript\"></script>
\t\t\t
\t\t\t<script type=\"text/javascript\">
\t\t\t\t\$(document).ready(function() {
\t\t\t\t\$('a.title').cluetip({splitTitle: '|'});
\t\t\t\t  \$('ol.rounded a:eq(0)').cluetip({splitTitle: '|', dropShadow: false, cluetipClass: 'rounded', showtitle: false});
\t\t\t\t  \$('ol.rounded a:eq(1)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'mouse'});
\t\t\t\t  \$('ol.rounded a:eq(2)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'bottomTop', topOffset: 70});
\t\t\t\t  \$('ol.rounded a:eq(3)').cluetip({cluetipClass: 'rounded', dropShadow: false, sticky: true, ajaxCache: false, arrows: true});
\t\t\t\t  \$('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});  
\t\t\t\t});
\t\t\t</script>
\t\t\t

\t\t\t\t";
        // line 91
        if ((isset($context["socialseo"]) ? $context["socialseo"] : null)) {
            echo (isset($context["socialseo"]) ? $context["socialseo"] : null);
        }
        echo " 
\t\t\t\t
\t\t\t\t";
        // line 93
        if ($this->getAttribute((isset($context["richsnippets"]) ? $context["richsnippets"] : null), "store", array())) {
            echo " 
<script type=\"application/ld+json\">
\t\t\t\t{ \"@context\" : \"http://schema.org\",
\t\t\t\t  \"@type\" : \"Organization\",
\t\t\t\t  \"name\" : \"";
            // line 97
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\",
\t\t\t\t  \"url\" : \"";
            // line 98
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\",
\t\t\t\t  \"logo\" : \"";
            // line 99
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\",
\t\t\t\t  \"contactPoint\" : [
\t\t\t\t\t{ \"@type\" : \"ContactPoint\",
\t\t\t\t\t  \"telephone\" : \"";
            // line 102
            echo (isset($context["telephone"]) ? $context["telephone"] : null);
            echo "\",
\t\t\t\t\t  \"contactType\" : \"customer service\"
\t\t\t\t\t} ] }
\t\t\t\t</script>
\t\t\t\t";
        }
        // line 106
        echo " \t\t\t\t
\t\t\t
</head>
<body class=\"";
        // line 109
        echo (isset($context["class"]) ? $context["class"] : null);
        echo (isset($context["basel_body_class"]) ? $context["basel_body_class"] : null);
        echo "\">
";
        // line 110
        $this->loadTemplate("basel/template/common/mobile-nav.twig", "basel/template/common/header.twig", 110)->display($context);
        // line 111
        echo "<div class=\"outer-container main-wrapper\">
";
        // line 112
        if ((isset($context["notification_status"]) ? $context["notification_status"] : null)) {
            // line 113
            echo "<div class=\"top_notificaiton\">
  <div class=\"container";
            // line 114
            if ((isset($context["top_promo_close"]) ? $context["top_promo_close"] : null)) {
                echo " has-close";
            }
            echo " ";
            echo (isset($context["top_promo_width"]) ? $context["top_promo_width"] : null);
            echo " ";
            echo (isset($context["top_promo_align"]) ? $context["top_promo_align"] : null);
            echo "\">
    <div class=\"table\">
    <div class=\"table-cell w100\"><div class=\"ellipsis-wrap\">";
            // line 116
            echo (isset($context["top_promo_text"]) ? $context["top_promo_text"] : null);
            echo "</div></div>
    ";
            // line 117
            if ((isset($context["top_promo_close"]) ? $context["top_promo_close"] : null)) {
                // line 118
                echo "    <div class=\"table-cell text-right\">
    <a onClick=\"addCookie('basel_top_promo', 1, 30);\$(this).closest('.top_notificaiton').slideUp();\" class=\"top_promo_close\">&times;</a>
    </div>
    ";
            }
            // line 122
            echo "    </div>
  </div>
</div>
";
        }
        // line 126
        $this->loadTemplate((("basel/template/common/headers/" . (isset($context["basel_header"]) ? $context["basel_header"] : null)) . ".twig"), "basel/template/common/header.twig", 126)->display($context);
        // line 127
        echo "<!-- breadcrumb -->
<div class=\"breadcrumb-holder\">
<div class=\"container\">
<span id=\"title-holder\">&nbsp;</span>
<div class=\"links-holder\">
<a class=\"basel-back-btn\" onClick=\"history.go(-1); return false;\"><i></i></a><span>&nbsp;</span>
</div>
</div>
</div>
<div class=\"container\">
";
        // line 137
        echo (isset($context["position_top"]) ? $context["position_top"] : null);
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "basel/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  346 => 137,  334 => 127,  332 => 126,  326 => 122,  320 => 118,  318 => 117,  314 => 116,  303 => 114,  300 => 113,  298 => 112,  295 => 111,  293 => 110,  288 => 109,  283 => 106,  275 => 102,  269 => 99,  265 => 98,  261 => 97,  254 => 93,  247 => 91,  229 => 75,  223 => 72,  219 => 70,  217 => 69,  211 => 66,  207 => 64,  205 => 63,  200 => 61,  197 => 60,  195 => 59,  190 => 57,  187 => 56,  185 => 55,  177 => 53,  173 => 52,  170 => 51,  158 => 48,  152 => 46,  150 => 45,  146 => 44,  143 => 43,  134 => 41,  130 => 40,  127 => 39,  123 => 37,  121 => 36,  108 => 34,  104 => 33,  99 => 31,  88 => 22,  82 => 21,  76 => 20,  69 => 18,  63 => 17,  55 => 14,  51 => 13,  47 => 12,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <!--<![endif]-->*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* {% if alternate %}{{ alternate }}{% endif %} 	*/
/* 			*/
/* */
/* {% if canonical_link %}{{ '<link href="'~canonical_link~'" rel="canonical" />' }}{% endif %} */
/* {% if robots %}{{ robots }}{% endif %} */
/* 			*/
/* {% if description %}<meta name="description" content="{{ description }}" />{% endif %}*/
/* {% if keywords %}<meta name="keywords" content= "{{ keywords }}" />{% endif %}*/
/* <!-- Load essential resources -->*/
/* <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>*/
/* <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />*/
/* <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>*/
/* <script src="catalog/view/theme/basel/js/slick.min.js"></script>*/
/* <script src="catalog/view/theme/basel/js/basel_common.js"></script>*/
/* <!-- Main stylesheet -->*/
/* <link href="catalog/view/theme/basel/stylesheet/stylesheet.css" rel="stylesheet">*/
/* <!-- Mandatory Theme Settings CSS -->*/
/* <style id="basel-mandatory-css">{{ basel_mandatory_css }}</style>*/
/* <!-- Plugin Stylesheet(s) -->*/
/* {% for style in styles %}*/
/* <link href="{{ style.href }}" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* {% endfor %}*/
/* {% if direction == 'rtl' %}*/
/* <link href="catalog/view/theme/basel/stylesheet/rtl.css" rel="stylesheet">*/
/* {% endif %}*/
/* <!-- Pluing scripts(s) -->*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}"></script>*/
/* {% endfor %}*/
/* <!-- Page specific meta information -->*/
/* {% for link in links %}*/
/* {% if link.rel == 'image' %}*/
/* <meta property="og:image" content="{{ link.href }}" />*/
/* {% else %}*/
/* <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* {% endif %}*/
/* {% endfor %}*/
/* <!-- Analytic tools -->*/
/* {% for analytic in analytics %}*/
/* {{ analytic }}*/
/* {% endfor %}*/
/* {% if basel_styles_status %}*/
/* <!-- Custom Color Scheme -->*/
/* <style id="basel-color-scheme">{{ basel_styles_cache }};</style>*/
/* {% endif %}*/
/* {% if basel_typo_status %}*/
/* <!-- Custom Fonts -->*/
/* <style id="basel-fonts">{{ basel_fonts_cache }}</style>*/
/* {% endif %}*/
/* {% if basel_custom_css_status %}*/
/* <!-- Custom CSS -->*/
/* <style id="basel-custom-css">*/
/* {{ basel_custom_css }}*/
/* </style>*/
/* {% endif %}*/
/* {% if basel_custom_js_status %}*/
/* <!-- Custom Javascript -->*/
/* <script>*/
/* {{ basel_custom_js }}*/
/* </script>*/
/* {% endif %}*/
/* */
/* 			<link rel="stylesheet" href="catalog/view/javascript/jquery.cluetip.css" type="text/css" />*/
/* 			<script src="catalog/view/javascript/jquery.cluetip.js" type="text/javascript"></script>*/
/* 			*/
/* 			<script type="text/javascript">*/
/* 				$(document).ready(function() {*/
/* 				$('a.title').cluetip({splitTitle: '|'});*/
/* 				  $('ol.rounded a:eq(0)').cluetip({splitTitle: '|', dropShadow: false, cluetipClass: 'rounded', showtitle: false});*/
/* 				  $('ol.rounded a:eq(1)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'mouse'});*/
/* 				  $('ol.rounded a:eq(2)').cluetip({cluetipClass: 'rounded', dropShadow: false, showtitle: false, positionBy: 'bottomTop', topOffset: 70});*/
/* 				  $('ol.rounded a:eq(3)').cluetip({cluetipClass: 'rounded', dropShadow: false, sticky: true, ajaxCache: false, arrows: true});*/
/* 				  $('ol.rounded a:eq(4)').cluetip({cluetipClass: 'rounded', dropShadow: false});  */
/* 				});*/
/* 			</script>*/
/* 			*/
/* */
/* 				{% if socialseo %}{{ socialseo }}{% endif %} */
/* 				*/
/* 				{% if richsnippets.store %} */
/* <script type="application/ld+json">*/
/* 				{ "@context" : "http://schema.org",*/
/* 				  "@type" : "Organization",*/
/* 				  "name" : "{{ name }}",*/
/* 				  "url" : "{{ home }}",*/
/* 				  "logo" : "{{ logo }}",*/
/* 				  "contactPoint" : [*/
/* 					{ "@type" : "ContactPoint",*/
/* 					  "telephone" : "{{ telephone }}",*/
/* 					  "contactType" : "customer service"*/
/* 					} ] }*/
/* 				</script>*/
/* 				{% endif %} 				*/
/* 			*/
/* </head>*/
/* <body class="{{ class }}{{ basel_body_class }}">*/
/* {% include 'basel/template/common/mobile-nav.twig' %}*/
/* <div class="outer-container main-wrapper">*/
/* {% if notification_status %}*/
/* <div class="top_notificaiton">*/
/*   <div class="container{% if top_promo_close %} has-close{% endif %} {{ top_promo_width }} {{ top_promo_align }}">*/
/*     <div class="table">*/
/*     <div class="table-cell w100"><div class="ellipsis-wrap">{{ top_promo_text }}</div></div>*/
/*     {% if top_promo_close %}*/
/*     <div class="table-cell text-right">*/
/*     <a onClick="addCookie('basel_top_promo', 1, 30);$(this).closest('.top_notificaiton').slideUp();" class="top_promo_close">&times;</a>*/
/*     </div>*/
/*     {% endif %}*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {% endif %}*/
/* {% include 'basel/template/common/headers/' ~ basel_header ~ '.twig' %}*/
/* <!-- breadcrumb -->*/
/* <div class="breadcrumb-holder">*/
/* <div class="container">*/
/* <span id="title-holder">&nbsp;</span>*/
/* <div class="links-holder">*/
/* <a class="basel-back-btn" onClick="history.go(-1); return false;"><i></i></a><span>&nbsp;</span>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* <div class="container">*/
/* {{ position_top }}*/
/* </div>*/
